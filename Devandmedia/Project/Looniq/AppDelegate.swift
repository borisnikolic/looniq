//
//  AppDelegate.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import UIKit
import Fabric
import Crashlytics
import MBProgressHUD
import FBSDKCoreKit
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let notificationManager = NotificationManager()
    let urlHandler = URLHandler()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]? = nil) -> Bool {
        Fabric.with([Crashlytics.self])
        GMSPlacesClient.provideAPIKey("AIzaSyA5uCF7pGHlndBgSiI3Bq3r39yfOXRClu4")
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)

        notificationManager.startUp()
        notificationManager.setupNotification()

        window = UIWindow(frame: UIScreen.main.bounds)
        if !UserDefaults.standard.bool(forKey: "facebookSplashscreenDone") {
            let onboardingView = UIStoryboard(name: "Onboarding", bundle: nil).instantiateViewController(withIdentifier: "Onboarding") as! OnboardingViewController
            window?.rootViewController = onboardingView

        } else {
            window?.rootViewController = HomeViewController()
        }
        window?.makeKeyAndVisible()

        return true
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        application.applicationIconBadgeNumber = 0

        guard let listView = (((self.window?.rootViewController as? HomeViewController)?.mainViewController as? UINavigationController)?.topViewController as? ListViewController) else { return }

        listView.setupEventsAndInfoBox()
    }

    @available(iOS 9, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey: Any] = [:]) -> Bool {
        return urlHandler.handleOpening(url: url, app: app, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    }

    @available(iOS 8, *)
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return urlHandler.handleOpening(url: url, app: application, sourceApplication: sourceApplication, annotation: annotation)
    }

    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        notificationManager.setupNotification(with: completionHandler)
    }
}
