//
//  Distance.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

struct Distance {
    let meter: Double
}

extension Distance: CustomStringConvertible {
    var description: String {
        if meter >= 10000 {
            return String(format: "%.0fkm", meter/1000)
        } else if meter >= 1000 {
            return String(format: "%.1fkm", meter/1000)
        } else {
            return String(format: "%.0fm", meter)
        }
    }
}

extension Distance: Hashable {
    var hashValue: Int {
        return Int(meter)
    }

    static func ==(lhs: Distance, rhs: Distance) -> Bool {
        return lhs.meter == rhs.meter
    }
}
