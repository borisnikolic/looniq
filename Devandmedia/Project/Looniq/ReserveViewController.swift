//
//  ReserveViewController.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import UIKit

class ReserveViewController: FormViewController {
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var anzahlTextField: UITextField!

    var eventId = Int()

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Reservierung".localized

        let sendButton = UIBarButtonItem(title: "Senden".localized, style: .done, target: self, action: #selector(WinViewController.sendButtonTapped(_:)))
        sendButton.tintColor = #colorLiteral(red: 0.4117647059, green: 0.5843137255, blue: 0.8509803922, alpha: 1)
        navigationItem.rightBarButtonItem = sendButton
    }

    @IBAction func stepperValueChanged(_ sender: UIStepper) {
        if Int(sender.value) == 0 {
            anzahlTextField.text = ""
        } else {
            anzahlTextField.text = "\(Int(sender.value))"
        }
    }

    func sendButtonTapped(_ sender: AnyObject) {
        let url = URL(string: looniqUrl + "reserve.php")
        guard var bodyData = extractTextViewValues() else { return }
        bodyData += "&reserveRequestFromApp=YES"
        sendPostRequest(to: url!, bodyData: bodyData, successText: "Danke!".localized)
    }

    func extractTextViewValues() -> String? {
        let nameText = nameTextField.text!
        let emailText = emailTextField.text!
        let numberText = numberTextField.text!
        let anzahlText = anzahlTextField.text!

        if numberText == "" && emailText == "" {
            showHUD(with: "Email oder Telefonnummer eingeben".localized)
            return nil
        }

        if numberText == "" && !isValidEmail(emailText) {
            showHUD(with: "Keine gültige Email".localized)
            return nil
        }

        if anzahlText == "" {
            showHUD(with: "Anzahl eingeben".localized)
            return nil
        }

        return "eventId=\(eventId)&name=\(nameText)&mail=\(emailText)&number=\(numberText)&anzahl=\(anzahlText)".encodeURIComponent()
    }
}
