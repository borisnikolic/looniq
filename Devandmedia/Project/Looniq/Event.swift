//
//  Event.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import UIKit

protocol EventImageDownloadDelegate {
    func imageDownloaded(for event: Event)
}

class Event: NSObject {
    let date: Date
    let endDate: Date?
    let id: Int
    let name: String
    let location: Location
    let text: String?
    let time: String
    let imageURL: String?
    var image: UIImage?
    let price: Double?
    let genre: String
    let priceClass: Int?
    let ad: Bool
    var win: Bool
    var favorite: Bool = false
    var ageRating: Int?
    var facebookEvent: String?
    var distance: Distance?

    var delegate: EventImageDownloadDelegate?

    override var description: String {
        return "\(id) - \(name) - \(location.name) - \(date)"
    }

    override init() {
        id = 0
        date = Date()
        endDate = nil
        location = Location()
        name = String()
        time = String()
        price = nil
        imageURL = nil
        ad = Bool()
        text = String()
        delegate = nil
        distance = nil
        win = Bool()
        genre = ""
        priceClass = nil
        ageRating = nil
        facebookEvent = nil
    }

    init(withDictionary dictionary: [String: AnyObject]) {
        id = Int(dictionary["id"] as! String)!

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = TimeZone(identifier: "GMT")

        date = formatter.date(from: dictionary["date"] as! String)!

        if let enddate = dictionary["enddate"] as? String {
            endDate = formatter.date(from: enddate)
        } else {
            endDate = nil
        }

        if let priceClassDb = dictionary["priceClass"] as? String {
            priceClass = Int(priceClassDb)
        } else {
            priceClass = nil
        }

        if let locationDict = dictionary["location"] as? [String: AnyObject] {
            var loc = Location(withDictionary: locationDict)
            loc.eventPriceClass = priceClass
            location = loc
        } else {
            location = Location()
        }

        if let name = dictionary["name"] as? String {
            self.name = name
        } else {
            self.name = ""
        }

        if let timeString = dictionary["time"] as? String {
            let index = timeString.index(timeString.endIndex, offsetBy: -3)
            time = timeString.substring(to: index)
        } else {
            time = ""
        }

        if let dictprice = dictionary["price"] as? String {
            price = Double(dictprice)
        } else {
            price = nil
        }

        var url = dictionary["imageurl"] as? String
        if url == "" {
            url = nil
        }
        imageURL = url

        if let adString = dictionary["ad"] as? String, let ad = Int(adString) {
            self.ad = ad == 1
        } else {
            self.ad = false
        }

        text = dictionary["description"] as? String

        if let distanceString = dictionary["distance"] as? String, let distanceDouble = Double(distanceString) {
            self.distance = Distance(meter: distanceDouble)
        } else {
            self.distance = nil
        }
        win = false
        if let winString = dictionary["win"] as? String, let win = Int(winString) {
            self.win = win == 1
        }

        if let genre = dictionary["genre"] as? String {
            self.genre = genre
        } else {
            self.genre = ""
        }

        if let ageRating = dictionary["ageRating"] as? String {
            self.ageRating = Int(ageRating)
        }

        var facebook = dictionary["facebookEvent"] as? String
        if facebook == "" {
            facebook = nil
        }
        facebookEvent = facebook
    }

    func downloadImage() {
        if image != nil { return }
        guard let imageURL = imageURL else { return }
        var jsonFileUrl: URL
        if imageURL.hasPrefix("http") {
            guard let url = URL(string: imageURL) else { return }
            jsonFileUrl = url
        } else {
            jsonFileUrl = URL(string: looniqUrl + "Eventbilder/\(imageURL)")!
        }

        let session = URLSession(configuration: BasicAuth.config)
        let task = session.downloadTask(with: jsonFileUrl) {
            url, response, error in
            guard let url = url else { return }
            var downloadedImage: UIImage?
            do {
                try downloadedImage = UIImage(data: Data(contentsOf: url))
            } catch let error {
                downloadedImage = nil
                print(error.localizedDescription)
                return
            }

            if downloadedImage == nil { return }
            self.image = downloadedImage
            self.delegate?.imageDownloaded(for: self)
        }
        task.resume()
    }
}
