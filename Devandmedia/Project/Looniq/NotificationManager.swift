//
//  NotificationManager.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

class NotificationManager {
    func startUp() {
        let app = UIApplication.shared
        app.registerUserNotificationSettings(UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil))

        app.setMinimumBackgroundFetchInterval(3 * 60 * 60)
        app.applicationIconBadgeNumber = 0
    }

    func setupNotification(with completionHandler: ((UIBackgroundFetchResult) -> Void)? = nil) {
        var components = Calendar.current.dateComponents([.hour, .day, .month, .year], from: Date())
        components.timeZone = TimeZone(abbreviation: "UTC")

        guard let hour = components.hour else {
            completionHandler?(.noData)
            return
        }

        let manager = EventManager()
        manager.downloadItems { events, error in
            guard error == nil, let events = events else {
                completionHandler?(.failed)
                return
            }

            guard events.count > 0 else {
                completionHandler?(.noData)
                return
            }

            let notification = UILocalNotification()
            if events.count == 1 {
                notification.alertBody = String.localizedStringWithFormat("%d Event heute! Schau nach".localized, events.count)
            } else {
                notification.alertBody = String.localizedStringWithFormat("%d Events heute! Schau nach".localized, events.count)
            }

            let fireTime = 17

            if hour >= fireTime {
                completionHandler?(.newData)
                return
            }

            components.hour = fireTime
            components.minute = 0
            components.second = 0

            guard let date = Calendar.current.date(from: components) else {
                completionHandler?(.newData)
                return
            }

            notification.fireDate = date
            notification.timeZone = TimeZone(abbreviation: "UTC")
            notification.soundName = UILocalNotificationDefaultSoundName
            notification.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1

            UIApplication.shared.cancelAllLocalNotifications()
            UIApplication.shared.scheduleLocalNotification(notification)
            
            completionHandler?(.newData)
        }
    }
}
