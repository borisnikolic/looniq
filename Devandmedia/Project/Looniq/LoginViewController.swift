//
//  LoginViewController.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Locksmith

class LoginViewController: UIViewController, FBSDKLoginButtonDelegate {

    var nameLabel = UILabel()
    var blurView = UIVisualEffectView()

    override func viewDidLoad() {
        super.viewDidLoad()

        view.frame = CGRect(x: 0, y: 0, width: 230, height: UIScreen.main.bounds.height)

        blurView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        blurView.frame = view.frame
        self.view.addSubview(blurView)

        if FBSDKAccessToken.current() == nil {
            if let dictionary = Locksmith.loadDataForUserAccount(userAccount: "me") {
                print(dictionary)
                if let tokenString = dictionary["tokenString"] as? String,
                    let appID = dictionary["appID"] as? String,
                    let userID = dictionary["userID"] as? String,
                    let expirationDate = dictionary["expirationDate"] as? Date,
                    let refreshDate = dictionary["refreshDate"] as? Date,
                    let firstName = dictionary["profileFirstName"] as? String,
                    let publishPermissions = dictionary["publishPermissions"] as? Bool {

                    var permissions: [String] = ["public_profile", "email", "user_events"]
                    if publishPermissions {
                        permissions.append("rsvp_event")
                    }

                    let token = FBSDKAccessToken(tokenString: tokenString, permissions: permissions, declinedPermissions: [], appID: appID, userID: userID, expirationDate: expirationDate, refreshDate: refreshDate)
                    FBSDKAccessToken.setCurrent(token)

                    FBSDKProfile.setCurrent(FBSDKProfile(userID: userID, firstName: firstName, middleName: "", lastName: "", name: "", linkURL: URL(fileURLWithPath: ""), refreshDate: Date()))
                }
            }
        }

        let loginButton = FBSDKLoginButton()
        loginButton.readPermissions = ["public_profile", "email", "user_events"]
        loginButton.center = CGPoint(x: self.view.bounds.midX, y: 200)
        loginButton.delegate = self
        view.addSubview(loginButton)

        let profilePic = FBSDKProfilePictureView(frame: CGRect(x: 20, y: 30, width: 70, height: 70))
        profilePic.profileID = "me"
        view.addSubview(profilePic)

        nameLabel.frame = CGRect(x: 110, y: 30, width: 100, height: 70)
        nameLabel.numberOfLines = 0
        nameLabel.adjustsFontSizeToFitWidth = true
        nameLabel.textColor = .white
        view.addSubview(nameLabel)

        profileChanged()

        FBSDKProfile.enableUpdates(onAccessTokenChange: true)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.profileChanged), name: NSNotification.Name.FBSDKProfileDidChange, object: nil)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        view.frame = CGRect(x: 0, y: 0, width: 230, height: UIScreen.main.bounds.height)
        blurView.frame = view.frame
    }

    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if result == nil || result.isCancelled || error != nil { return }
        print("Logged in")
    }

    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        try? Locksmith.deleteDataForUserAccount(userAccount: "me")
    }

    func profileChanged() {
        if FBSDKProfile.current() != nil, let name = FBSDKProfile.current().firstName {
            let token = FBSDKAccessToken.current()!

            do {
                let dict = ["tokenString": token.tokenString, "appID": token.appID, "userID": token.userID, "expirationDate": token.expirationDate, "refreshDate": token.refreshDate, "publishPermissions": false, "profileFirstName": name] as [String: Any]
                try Locksmith.saveData(data: dict, forUserAccount: "me")
            } catch let error {
                print(error.localizedDescription)
            }
            nameLabel.text = "Hallo".localized + " " + name + "!"
        } else {
            nameLabel.text = ""
        }
    }
}
