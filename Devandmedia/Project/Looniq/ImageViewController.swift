//
//  ImageViewController.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import Foundation

class ImageViewController: UIViewController {
    var image: UIImage?
    var scrollView = UIScrollView()
    var imageView = UIImageView()

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        self.image = nil
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    init(with image: UIImage) {
        self.image = image
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        imageView = UIImageView(image: image)
        imageView.backgroundColor = .black
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true

        scrollView = UIScrollView(frame: view.frame)
        scrollView.minimumZoomScale = 1
        scrollView.maximumZoomScale = 5
        scrollView.delegate = self
        scrollView.addSubview(imageView)
        view.addSubview(scrollView)

        updateViews()

        imageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissFullscreenImage))
        navigationItem.hidesBackButton = true

        navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.5369516262, green: 0.6873196121, blue: 0.9010033965, alpha: 1)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        navigationController?.navigationBar.tintColor = .white
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        updateViews()
    }

    func updateViews() {
        let height = UIApplication.shared.statusBarFrame.height + (navigationController?.navigationBar.frame.size.height ?? 44)

        imageView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height - height)
        scrollView.frame = view.frame
        scrollView.contentSize = imageView.frame.size
    }

    func dismissFullscreenImage() {
        _ = navigationController?.popViewController(animated: true)

        guard let home = (UIApplication.shared.keyWindow!.rootViewController as? HomeViewController) else { return }
        home.rightContainerView.frame.origin.x = view.bounds.width
    }
}

extension ImageViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
}
