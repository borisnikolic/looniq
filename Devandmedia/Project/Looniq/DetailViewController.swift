//
//  DetailViewController.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import MapKit
import MBProgressHUD
import FBSDKCoreKit
import FBSDKLoginKit
import Locksmith

class DetailViewController: BaseViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, EventImageDownloadDelegate, URLSessionDelegate {
    var details: Event

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var eventnameLabel: UILabel!
    @IBOutlet weak var detailMapView: MKMapView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var winButton: UIButton!
    @IBOutlet weak var reserveButton: UIButton!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var alterLabel: UILabel!
    @IBOutlet weak var dresscodeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    @IBOutlet weak var attendButton: UIButton!
    @IBOutlet weak var attendeesLabel: UILabel!
    @IBOutlet weak var looniqAttendeesLabel: UILabel!

    @IBOutlet weak var priceSmallLabel: UILabel!
    @IBOutlet weak var ageSmallLabel: UILabel!
    @IBOutlet weak var dressSmallLabel: UILabel!

    var internalCounter = 0

    var gradientView = UIImageView()

    let mapView = MapUIView()

    init(with event: Event) {
        details = event
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        details = Event()
        super.init(coder: aDecoder)
    }
    
    @IBAction func locationButtonTapped(_ sender: AnyObject) {
        let locationDetailView = UIStoryboard(name: "LocationDetail", bundle: nil).instantiateViewController(withIdentifier: "LocationDetail") as! LocationDetailViewController
        locationDetailView.location = details.location
        navigationController?.pushViewController(locationDetailView, animated: true)
    }

    @IBAction func goToImageButtonTapped(_ sender: AnyObject) {
        let locationDetailView = UIStoryboard(name: "LocationDetail", bundle: nil).instantiateViewController(withIdentifier: "LocationDetail") as! LocationDetailViewController
        locationDetailView.location = details.location
        locationDetailView.scrollToImages = true
        navigationController?.pushViewController(locationDetailView, animated: true)
    }

    @IBAction func winButtonTapped(_ sender: AnyObject) {
        if details.win {
            let winView = UIStoryboard(name: "Win", bundle: nil).instantiateViewController(withIdentifier: "Win") as! WinViewController
            winView.eventId = details.id
            navigationController?.pushViewController(winView, animated: true)
        }
    }

    @IBAction func reserveButtonTapped(_ sender: AnyObject) {
        let reserveView = UIStoryboard(name: "Reserve", bundle: nil).instantiateViewController(withIdentifier: "Reserve") as! ReserveViewController
        reserveView.eventId = details.id
        navigationController?.pushViewController(reserveView, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        attendeesLabel.text = ""
        looniqAttendeesLabel.text = ""

        scrollView.bounds = self.view.bounds
        scrollView.delegate = self
        scrollView.backgroundColor = UIColor.clear
        self.addBackgroundImageToView()
        self.view.sendSubview(toBack: headerImage)
        scrollView.contentSize = self.view.bounds.size

        if let image = details.image {
            imageView.image = image

            gradientView = UIImageView(image: image.imageWithBottomToTopGradientOverlay())
        }
        gradientView.contentMode = .scaleAspectFill
        gradientView.clipsToBounds = true
        gradientView.frame = imageView.frame
        gradientView.isUserInteractionEnabled = false
        imageView.addSubview(gradientView)

        setupLabels()

        details.delegate = self
        details.downloadImage()

        let dateFormatter = DateFormatter()
        dateFormatter.setLocalizedDateFormatFromTemplate("MMd")
        var dateText = dateFormatter.string(from: details.date)

        if let endDate = details.endDate {
            dateText += " - " + dateFormatter.string(from: endDate)
        }

        dateLabel.text = dateText

        let shareButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(DetailViewController.shareAction(_:)))
        self.navigationItem.rightBarButtonItem = shareButton

        self.title = "Event"
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        gradientView.frame = imageView.frame
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        detailMapView.showsUserLocation = true
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        detailMapView.mapType = .hybrid
        detailMapView.mapType = .standard
        detailMapView.showsUserLocation = false
        detailMapView.delegate = nil
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        gradientView.frame = imageView.frame
        mapView.frame = detailMapView.frame
    }

    func setupLabels() {
        eventnameLabel.text = details.name
        locationButton.setTitle(details.location.name + " ▹", for: .normal)
        timeLabel.text = "\(details.time)"
        if let hours = details.location.openingHours {
            if hours.isOpen {
                timeLabel.textColor = .green
            } else {
                timeLabel.textColor = .red
            }
        }

        var text = String()
        if let price = details.price {
            text = String(format: "%2.0lf,-", price)
        }
        priceLabel.text = text
        if priceLabel.text == "" || priceLabel.text == nil {
            priceSmallLabel.text = ""
        }

        infoLabel.numberOfLines = 0
        infoLabel.lineBreakMode = .byWordWrapping
        if details.text == "" || details.text == nil {
            infoLabel.font = UIFont.italicSystemFont(ofSize: infoLabel.font.pointSize)
            infoLabel.text = "Derzeit sind uns keine Infos bekannt.".localized
        } else {
            infoLabel.text = details.text
        }

        categoryLabel.text = details.genre

        if let age = details.ageRating {
            alterLabel.text = "\(age)"
        } else {
            ageSmallLabel.text = ""
            alterLabel.text = ""
        }

        dresscodeLabel.text = details.location.dresscode
        if dresscodeLabel.text == "" || dresscodeLabel.text == nil {
            dressSmallLabel.text = ""
        }

        if !details.win {
            winButton.isHidden = true
            let horizontalConstraint = NSLayoutConstraint(item: reserveButton, attribute: .trailing, relatedBy: .equal, toItem: reserveButton.superview!, attribute: .trailing, multiplier: 1, constant: -18)
            view.addConstraint(horizontalConstraint)
        }

        if details.location.mail == nil {
            reserveButton.isHidden = true
        }

        detailMapView.showsUserLocation = true

        let latitude = details.location.latitude
        let longitude = details.location.longitude
        let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)

        let viewRegion = MKCoordinateRegionMakeWithDistance(coordinate, 750, 750)
        detailMapView.setRegion(viewRegion, animated: true)
        detailMapView.showsUserLocation = true

        let pin = MKPointAnnotation()
        pin.coordinate = coordinate
        pin.title = details.location.name

        detailMapView.addAnnotation(pin)
        detailMapView.selectAnnotation(pin, animated: true)
        mapView.location = details.location

        scrollView.insertSubview(mapView, aboveSubview: detailMapView)

        getCounter()

        if UserDefaults.standard.bool(forKey: "InternalEvent\(details.id)") {
            attendButton.setTitle("Du nimmst teil!".localized, for: .normal)
        }

        guard let facebookEvent = details.facebookEvent else { return }
        self.attendeesLabel.text = ""
        let request = FBSDKGraphRequest(graphPath: "\(facebookEvent)/attending", parameters: nil, httpMethod: "GET")
        _ = request?.start { _, response, error in
            print(response ?? "")
            print(error?.localizedDescription ?? "")

            guard let dict = response as? NSDictionary else { return }
            guard let attendees = dict["data"] as? [NSDictionary] else { return }

            var attendeeText = ""

            if attendees.count > 0 {
                attendeeText += attendees[0]["name"]! as! String
                if attendees.count == 1 {
                    attendeeText += " nimmt teil!".localized
                } else if attendees.count == 2 {
                    attendeeText += " und ".localized + (attendees[1]["name"]! as! String) + " nehmen teil!".localized
                } else if attendees.count > 2 {
                    attendeeText += ", " + (attendees[1]["name"]! as! String) + " und ".localized + "\(attendees.count-2)" + " weitere Personen nehmen teil!".localized
                }
            }

            let userId = FBSDKAccessToken.current().userID
            for attendee in attendees {
                if let attendeeId = attendee["id"] as? String, attendeeId == userId {
                    self.attendButton.setTitle("Du nimmst teil!".localized, for: .normal)
                    UserDefaults.standard.set(true, forKey: "FacebookEvent\(self.details.id)")
                    self.incrementCounter()
                    self.attendButton.isUserInteractionEnabled = false
                }
            }

            self.attendeesLabel.text = attendeeText
        }
    }

    func imageDownloaded(for event: Event) {
        print("Finished downloading image!")

        DispatchQueue.main.async {
            self.imageView.image = self.details.image

            self.gradientView.image = self.details.image?.imageWithBottomToTopGradientOverlay()
        }
    }

    @IBAction func uploadImageButtonTapped(_ sender: AnyObject) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let action = UIAlertController(title: "Lade ein Bild hoch".localized, message: nil, preferredStyle: .actionSheet)

            let cameraAction = UIAlertAction(title: "Kamera".localized, style: .default) { action in
                self.showImagePicker(for: .camera)
            }
            let galleryAction = UIAlertAction(title: "Galerie".localized, style: .default) { action in
                self.showImagePicker(for: .photoLibrary)
            }
            let cancelAction = UIAlertAction(title: "Abbrechen".localized, style: .cancel)

            action.addAction(cameraAction)
            action.addAction(galleryAction)
            action.addAction(cancelAction)

            if let button = sender as? UIButton {
                action.popoverPresentationController?.sourceView = button
                action.popoverPresentationController?.sourceRect = button.bounds
            }

            present(action, animated: true)
        } else {
            showImagePicker(for: .photoLibrary)
        }
    }

    func showImagePicker(for sourceType: UIImagePickerControllerSourceType) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = sourceType
        imagePicker.allowsEditing = true
        imagePicker.mediaTypes = ["public.image"]
        present(imagePicker, animated: true)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        picker.dismiss(animated: true, completion: nil)

        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        let imageData = UIImageJPEGRepresentation(image, 0.2)
        let url = URL(string: looniqUrl + "upload_image.php")!

        var request = URLRequest(url: url)
        request.httpMethod = "POST"

        let boundary = "---------------------------14737809831466499882746641449"
        let contentType = "multipart/form-data; boundary=\(boundary)"
        request.addValue(contentType, forHTTPHeaderField: "Content-Type")

        var body = Data()

        body.append("--\(boundary)\r\n".data(using: .utf8)!)
        body.append("Content-Disposition: form-data; name=\"location\"\r\n\r\n".data(using: .utf8)!)
        body.append("\(details.location.name)\r\n".data(using: .utf8)!)

        body.append("--\(boundary)\r\n".data(using: .utf8)!)
        body.append("Content-Disposition: form-data; name=\"uploadedfile\"; filename=\"uploaded.jpeg\"\r\n".data(using: .utf8)!)
        body.append("Content-Type: image/jpeg\r\n\r\n".data(using: .utf8)!)
        body.append(imageData!)
        body.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)

        request.httpBody = body

        let session = URLSession(configuration: BasicAuth.config)
        let task = session.dataTask(with: request) {
            data, response, error in
            DispatchQueue.main.async {
                let hud = MBProgressHUD.showAdded(to: UIApplication.shared.keyWindow!, animated: true)
                hud.mode = .customView
                let image = UIImage(named: "checkmark")?.withRenderingMode(.alwaysTemplate)
                hud.customView = UIImageView(image: image)
                hud.isSquare = true
                hud.isUserInteractionEnabled = false
                hud.label.text = "Bild hochgeladen".localized
                hud.detailsLabel.text = "Wird nach 90 Tagen gelöscht".localized
                hud.hide(animated: true, afterDelay: 5.0)
            }
        }
        task.resume()
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }

    @IBAction func acceptButtonTapped(_ sender: AnyObject) {
        var status: String
        if attendButton.titleLabel?.text == "Teilnehmen".localized {
            status = "attending"
        } else {
            status = "declined"
        }

        func facebookRequest() {
            guard let facebookEvent = self.details.facebookEvent else { return }
            let request = FBSDKGraphRequest(graphPath: "\(facebookEvent)/\(status)", parameters: nil, httpMethod: "POST")
            _ = request?.start { _, response, error in

                if let succeeded = (response as? NSDictionary)?["success"] as? Bool {
                    if succeeded {
                        if status == "attending" {
                            self.attendButton.setTitle("Du nimmst teil!".localized, for: .normal)
                            UserDefaults.standard.set(true, forKey: "FacebookEvent\(self.details.id)")
                            self.attendButton.isUserInteractionEnabled = false
                        } else {
                            self.attendButton.setTitle("Teilnehmen".localized, for: .normal)
                        }

                        self.attendeesLabel.text = ""
                        let request2 = FBSDKGraphRequest(graphPath: "\(facebookEvent)/attending", parameters: nil, httpMethod: "GET")
                        _ = request2?.start { _, response2, error2 in
                            print(response2 ?? "")
                            print(error2?.localizedDescription ?? "")

                            guard let dict = response2 as? NSDictionary else { return }
                            guard let attendees = dict["data"] as? [NSDictionary] else { return }

                            var attendeeText = ""

                            if attendees.count > 0 {
                                attendeeText += attendees[0]["name"]! as! String
                                if attendees.count == 1 {
                                    attendeeText += " nimmt teil!".localized
                                } else if attendees.count == 2 {
                                    attendeeText += " und ".localized + (attendees[1]["name"]! as! String) + " nehmen teil!".localized
                                } else if attendees.count > 2 {
                                    attendeeText += ", " + (attendees[1]["name"]! as! String) + " und ".localized + "\(attendees.count-2)" + " weitere Personen nehmen teil!".localized
                                }
                            }

                            self.attendeesLabel.text = attendeeText
                        }
                    }
                }
            }
        }

        if status == "declined" {
            return
        }

        incrementCounter()

        guard let _ = self.details.facebookEvent else {
            self.attendButton.setTitle("Du nimmst teil!".localized, for: .normal)
            return
        }

        guard FBSDKAccessToken.current() != nil else { return }

        if FBSDKAccessToken.current().permissions.contains(AnyHashable("rsvp_event")) {
            facebookRequest()
        } else {
            let manager = FBSDKLoginManager()
            manager.logIn(withPublishPermissions: ["rsvp_event"], from: self) { loginResult, error in
                if error != nil {
                    print(error?.localizedDescription ?? "")
                    return
                }

                if let name = FBSDKProfile.current().firstName, let token = FBSDKAccessToken.current() {
                    do {
                        let dict = ["tokenString": token.tokenString, "appID": token.appID, "userID": token.userID, "expirationDate": token.expirationDate, "refreshDate": token.refreshDate, "publishPermissions": true, "profileFirstName": name] as [String: Any]
                        try Locksmith.updateData(data: dict, forUserAccount: "me")
                    } catch let error {
                        print(error.localizedDescription)
                    }
                }

                facebookRequest()
            }
        }
    }

    func getCounter() {
        let jsonFileUrl = URL(string: looniqUrl + "counterForEvent.php")!
        let postMessage = "counterRequestApp=YES&eventId=\(details.id)"

        var request = URLRequest(url: jsonFileUrl)
        request.httpBody = postMessage.data(using: .utf8)
        request.httpMethod = "POST"

        let session = URLSession(configuration: BasicAuth.config)
        let task = session.dataTask(with: request) {
            data, response, error in
            if error != nil {
                print(error?.localizedDescription ?? "")
                return
            }

            guard let data = data, let counterString = String(data: data, encoding: .utf8), let counter = Int(counterString) else { return }

            self.internalCounter = counter
            DispatchQueue.main.async {
                self.setLooniqAttendeesLabel(for: self.internalCounter)
            }
        }
        task.resume()
    }

    func setLooniqAttendeesLabel(for count: Int) {
        guard count > 0 else { return }
        if count == 1 {
            self.looniqAttendeesLabel.text = String.localizedStringWithFormat("Es nimmt %d Looniq User teil!".localized, self.internalCounter)
        } else {
            self.looniqAttendeesLabel.text = String.localizedStringWithFormat("Es nehmen %d Looniq User teil!".localized, self.internalCounter)
        }
    }

    func incrementCounter() {
        guard !UserDefaults.standard.bool(forKey: "InternalEvent\(details.id)") else { return }
        let jsonFileUrl = URL(string: looniqUrl + "incrementCounterForEvent.php")!
        let postMessage = "incrementRequestApp=YES&eventId=\(details.id)"

        var request = URLRequest(url: jsonFileUrl)
        request.httpBody = postMessage.data(using: .utf8)
        request.httpMethod = "POST"

        let session = URLSession(configuration: BasicAuth.config)
        let task = session.dataTask(with: request) {
            data, response, error in
            if error != nil {
                print(error?.localizedDescription ?? "")
                return
            }
            guard let data = data else { return }

            UserDefaults.standard.set(true, forKey: "InternalEvent\(self.details.id)")

            self.internalCounter = Int(String(data: data, encoding: .utf8)!)!
            DispatchQueue.main.async {
                self.setLooniqAttendeesLabel(for: self.internalCounter)
            }
        }
        task.resume()
    }

    @IBAction func imageTapped(sender: UITapGestureRecognizer) {
        guard let image = (sender.view as? UIImageView)?.image else { return }
        let imageViewController = ImageViewController(with: image)
        navigationController?.pushViewController(imageViewController, animated: true)
    }

    func shareAction(_ sender: UIBarButtonItem) {
        let shareString = "Komm mit Looniq und mir zu ".localized + "\(details.name) - \(details.location.name)\n\nlooniq://event?\(details.id)"

        let activityVC = UIActivityViewController(activityItems: [shareString], applicationActivities: nil)
        activityVC.excludedActivityTypes = []
        activityVC.popoverPresentationController?.barButtonItem = sender

        self.present(activityVC, animated: true, completion: nil)
    }
}
