//
//  EventManager.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import Foundation
import CoreLocation

protocol EventDownloadDelegate {
    func eventsDownloaded(_ events: [Event])
    func eventsNotDownloaded(reason: String)
}

extension EventDownloadDelegate {
    func eventsNotDownloaded(reason: String) {}
}

protocol LocationDownloadDelegate {
    func locationsDownloaded(_ locations: [Location])
}

protocol LocationUpdateDelegate {
    func updateLocationDistance(_ refreshIds: [Int], events: [Event])
}

class EventManager: NSObject {
    // MARK: - Properties
    var allPastAndFutureEvents = [Event]()
    var allTodayEvents = [Event]()
    var allFutureEvents = [Event]()

    var allLocations = [Location]()
    var favoriteLocations: [String] = []

    var delegate: EventDownloadDelegate?

    var locationDelegate: LocationUpdateDelegate?

    var locationDownloadDelegate: LocationDownloadDelegate?

    var locationEnabled: Bool {
        return CLLocationManager.authorizationStatus() != .denied
    }

    var currentLocaleIndependentDate: Date {
        let date = DateFormatter()
        date.dateFormat = "dd.MM.yyyy"
        date.calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        date.locale = Locale(identifier: "en_US_POSIX")

        let hoursUntilEventsAreShown = 4.0
        let timeInterval = Date().timeIntervalSinceReferenceDate - 3600 * hoursUntilEventsAreShown
        return Date(timeIntervalSinceReferenceDate: timeInterval)
    }

    // MARK: - Initializer
    override init() {
        super.init()
        delegate = nil
    }

    // MARK: - Setup Methods
    func setupEvents() {
        downloadItems()

        if UserDefaults.standard.bool(forKey: "simulateEvents") || ProcessInfo.processInfo.arguments.contains("-simulateEvents YES") {
            setupSimulatedEvents()
        }

        favoriteLocations = readLocations()
    }

    func setupSimulatedEvents() {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let dateString = formatter.string(from: Date())

        let eventDictionary1 = ["id":"5", "price":"7", "ad":"1", "time":"22:00:00", "date":dateString, "description":"Das größte Clubbing in ganz Wien! Schaut vorbei!", "name":"Big Clubbing", "distance":"250", "genre":"Rock", "win":"1", "priceClass":"2", "location":["id":"5","name":"FlashClub","address":"Graben 14","latitude":"48.208867","longitude":"16.369452", "mail":"test@test.at"]] as [String : Any]
        let event1 = Event(withDictionary: eventDictionary1 as [String: AnyObject])
        event1.favorite = true
        allTodayEvents.append(event1)

//        let eventDictionary2 = ["id":"8", "price":"9", "ad":"0", "time":"21:00:00", "date":dateString, "description":"Dies ist ein Testevent", "name":"Tornado", "locationName":"Public 8", "distance":"3400", "imageurl":"011220152.jpg", "genre":"Pop", "win":"1"]
//        let event2 = Event(withDictionary: eventDictionary2 as [String : AnyObject])
//        allTodayEvents.append(event2)
//
//        let eventDictionary3 = ["id":"3", "price":"7", "ad":"0", "time":"23:00:00", "date":dateString, "description":"Dies ist ein Testevent", "name":"Over Clubbing", "locationName":"DanceClub", "distance":"7540", "genre":"Pop"]
//        let event3 = Event(withDictionary: eventDictionary3 as [String : AnyObject])
//        event3.favorite = true
//        allTodayEvents.append(event3)
//
//        let eventDictionary4 = ["id":"4", "price":"8", "ad":"0", "time":"21:00:00", "date":dateString, "description":"Dies ist ein Testevent", "name":"Crazy Night", "locationName":"Salzclub", "distance":"10000", "genre":"Hip Hop"]
//        let event4 = Event(withDictionary: eventDictionary4 as [String : AnyObject])
//        allTodayEvents.append(event4)
    }

    // MARK: - User Defaults
    func readLocations() -> [String] {
        guard let locations = UserDefaults.standard.stringArray(forKey: "locations") else {
            return []
        }
        print("Read favorite locations:\(locations)")
        return locations
    }

    func save(_ locations: [String]) {
        UserDefaults.standard.set(locations, forKey: "locations")
        print(locations)
    }

    // MARK: - Helper Methods
    func filterEvents(for search: String) -> [Event] {
        let nearEvents = getNearEvents(forToday: false)
        return nearEvents.filter { event in
            return event.name.lowercased().contains(search.lowercased()) || event.location.name.lowercased().contains(search.lowercased())
        }
    }

    func filterLocations(for search: String) -> [Location] {
        let locations = allLocations
        return locations.filter { location in
            return location.name.lowercased().contains(search.lowercased())
        }
    }

    func getFavoriteEvents() -> [Event] {
        return allTodayEvents.filter { event in
            for location in favoriteLocations {
                if event.location.name == location {
                    return true
                }
            }
            return false
        }
    }

    func getNearEvents(forToday: Bool) -> [Event] {
        var allEvents = [Event]()
        if forToday {
            allEvents = allTodayEvents
        } else {
            allEvents = allFutureEvents
        }

        let nearEvents = allEvents.sorted {
            guard let ldist = $0.distance, let rdist = $1.distance else { return false }
            return ldist.meter < rdist.meter
        }

        return EventFilter.sharedInstance.sort(events: nearEvents)
    }

    func getCategorySortedDictionary() -> [String: [Event]] {
        var dict = [String: [Event]]()
        getNearEvents(forToday: true).forEach {
            let category = $0.genre
            // Check if key already exists in dictionary
            if dict[category] != nil {
                dict[category]?.append($0)
            } else {
                dict[category] = [$0]
            }
        }
        return dict
    }

    func getAllCurrentMusicCategories() -> [String] {
        var allCategories = [String]()
        getNearEvents(forToday: true).forEach {
            let category = $0.genre
            if !allCategories.contains(category) {
                allCategories.append(category)
            }
        }

        allCategories.sort { $0 < $1 }
        return allCategories
    }

    func toggleFavorite(for location: String) {
        for i in 0..<allTodayEvents.count {
            if location == allTodayEvents[i].location.name {
                if allTodayEvents[i].favorite {
                    allTodayEvents[i].favorite = false
                    favoriteLocations = favoriteLocations.filter { $0 != allTodayEvents[i].location.name }
                    save(favoriteLocations)
                } else {
                    allTodayEvents[i].favorite = true
                    if favoriteLocations.index(of: allTodayEvents[i].location.name) == nil {
                        favoriteLocations.append(allTodayEvents[i].location.name)
                    }
                    save(favoriteLocations)
                }
            }
        }
    }

    func setFavoritesForTodayEvents() {
        for location in favoriteLocations {
            for j in 0..<allTodayEvents.count {
                if allTodayEvents[j].location.name == location {
                    allTodayEvents[j].favorite = true
                }
            }
        }
    }

    func getEvents(from dict: [[String: AnyObject]]) -> [Event] {
        var events = [Event]()

        for jsonEvent in dict {
            let event = Event(withDictionary: jsonEvent)

            events.append(event)
        }

        return events
    }

    func getLocations(from dict: [[String: AnyObject]]) -> [Location] {
        var locations = [Location]()

        for jsonLocation in dict {
            let location = Location(withDictionary: jsonLocation)

            locations.append(location)
        }

        return locations
    }

    func downloadItems(completionHandler: (([Event]?, Error?) -> Void)? = nil) {
        let jsonFileUrl = URL(string: looniqUrl + "events.php")!

        let session = URLSession(configuration: BasicAuth.config)
        let task = session.dataTask(with: jsonFileUrl) {
            data, response, error in

            if error != nil {
                print(error?.localizedDescription ?? "")
                self.delegate?.eventsNotDownloaded(reason: "Internet nicht erreichbar")
                completionHandler?(nil, error)
                return
            }

            guard let data = data else {
                completionHandler?(nil, nil)
                return
            }
            var events = [Event]()
            var jsonArray: [[String: AnyObject]]

            // Parse downloaded data into JSON Dictionary
            do {
                jsonArray = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [[String: AnyObject]]
            } catch let jsonerror {
                print("json error: \(jsonerror)")
                self.delegate?.eventsDownloaded([])
                completionHandler?(nil, jsonerror)
                return
            }

            print(jsonArray)

            events = self.getEvents(from: jsonArray)
            self.allPastAndFutureEvents = events

            let todayEventsArray = events.filter { event in
                let cal = Calendar(identifier: .gregorian)

                if let endDate = event.endDate {
                    // Must be same or ascending
                    let startOrder = cal.compare(event.date, to: self.currentLocaleIndependentDate, toGranularity: .day)
                    // Must be same or descending
                    let endOrder = cal.compare(endDate, to: self.currentLocaleIndependentDate, toGranularity: .day)

                    if startOrder != .orderedDescending && endOrder != .orderedAscending {
                        return true
                    } else {
                        return false
                    }

                } else {
                    return cal.isDate(event.date, inSameDayAs: self.currentLocaleIndependentDate)
                }
            }

            self.allTodayEvents = todayEventsArray

            let futureEventsArray = events.filter({ event in
                let cal = Calendar(identifier: .gregorian)
                let order = cal.compare(event.date, to: self.currentLocaleIndependentDate, toGranularity: .day)
                if order == .orderedDescending {
                    return true
                } else {
                    return false
                }
            })

            self.allFutureEvents = todayEventsArray + futureEventsArray

            print(self.allTodayEvents)
            if UserDefaults.standard.bool(forKey: "simulateEvents") || ProcessInfo.processInfo.arguments.contains("-simulateEvents YES") {
                self.setupSimulatedEvents()
            }

            self.setFavoritesForTodayEvents()

            completionHandler?(self.allTodayEvents, nil)
            self.delegate?.eventsDownloaded(self.getNearEvents(forToday: true))
        }
        task.resume()
    }

    func downloadLocations() {
        let jsonFileUrl = URL(string: looniqUrl + "locations.php")!

        let session = URLSession(configuration: BasicAuth.config)
        let task = session.dataTask(with: jsonFileUrl) {
            data, response, error in

            if error != nil {
                print(error?.localizedDescription ?? "")
                return
            }

            guard let data = data else { return }
            var jsonArray: [[String: AnyObject]]

            do {
                jsonArray = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [[String: AnyObject]]
            } catch let jsonerror {
                print("json error: \(jsonerror)")
                return
            }

            self.allLocations = self.getLocations(from: jsonArray)

            self.locationDownloadDelegate?.locationsDownloaded(self.self.allLocations)
        }
        task.resume()
    }

    // MARK: - Location Updates
    func startLocationUpdates() {
        LocationManager.sharedManager.eventDelegate = self
        LocationManager.sharedManager.startLocationUpdates()
    }

    func distanceInRange(_ first: Double, second: Double, range: Double) -> Bool {
        var far = second
        var near = first

        if first > second {
            far = first
            near = second
        }

        if far-near <= range {
            return true
        }

        return false
    }
}

extension EventManager: LocationManagerDelegate {
    func didUpdateLocation(_ location: CLLocation) {
        var refreshIds = [Int]()

        for i in 0..<allFutureEvents.count {
            let lat = allFutureEvents[i].location.latitude
            let long = allFutureEvents[i].location.longitude
            let newDistance = location.distance(from: CLLocation(latitude: lat, longitude: long))

            if let oldDistance = allFutureEvents[i].distance {
                if newDistance > 10000 && !distanceInRange(oldDistance.meter, second: newDistance, range: 1000.0) {
                    print(String(format: "Distance to \(allFutureEvents[i].location.name) is %.0fkm. And range to previous distance is greater 1km.", newDistance / 1000))
                    refreshIds.append(allFutureEvents[i].id)

                } else if newDistance > 1000 && !distanceInRange(oldDistance.meter, second: newDistance, range: 100.0) {
                    print(String(format: "Distance to \(allFutureEvents[i].location.name) is %.1fkm. And range to previous distance is greater 100m.", newDistance / 1000))
                    refreshIds.append(allFutureEvents[i].id)

                } else if newDistance > 100 && !distanceInRange(oldDistance.meter, second: newDistance, range: 10.0) {
                    print(String(format: "Distance to \(allFutureEvents[i].location.name) is %.0fm. And range to previous distance is greater 10m.", newDistance))
                    refreshIds.append(allFutureEvents[i].id)
                }
            } else {
                print("No previous distance set for \(allFutureEvents[i].location.name)")
            }

            allFutureEvents[i].distance = Distance(meter: newDistance)
        }

        setFavoritesForTodayEvents()

        locationDelegate?.updateLocationDistance(refreshIds, events: getNearEvents(forToday: true))
    }
}
