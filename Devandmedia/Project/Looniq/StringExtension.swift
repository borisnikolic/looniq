//
//  StringExtension.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }

    func encodeURIComponent() -> String {
        guard let encodedUrl = self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else { return "" }
        let moreEncodedUrl = encodedUrl.components(separatedBy: "+").joined(separator: "%2B")
        return moreEncodedUrl
    }
}
