//
//  TimeManager.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import Foundation

protocol TimeManagerDelegate {
    func updateClock()
}

class TimeManager: NSObject {
    var target: TimeManagerDelegate

    class var currentDate: String {
        let date = DateFormatter()

        let formatString = DateFormatter.dateFormat(fromTemplate: "EdMMM", options: 0, locale: Locale.current)
        date.dateFormat = formatString
        date.dateStyle = .full
        date.timeStyle = .none
        date.timeZone = TimeZone.current

        return date.string(from: Date())
    }

    class var currentLocaleIndependentTime: String {
        let time = DateFormatter()
        time.dateFormat = "HH:mm"
        return time.string(from: Date())
    }

    init(target: TimeManagerDelegate) {
        self.target = target
        super.init()
        setupClockAndDateUpdates()
    }

    // MARK: - Time & Date Related
    func setupClockAndDateUpdates() {
        let date = DateFormatter()
        date.dateFormat = "ss"
        date.timeZone = TimeZone.current
        let secondString = date.string(from: Date())
        guard let seconds = Int(secondString) else { return }

        let tillNextMinute = (60 - seconds) % 60
        Timer.scheduledTimer(timeInterval: Double(tillNextMinute+1), target: self, selector: #selector(TimeManager.startClockTimer), userInfo: nil, repeats: false)
    }

    func startClockTimer() {
        updateTargetClock()
        Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(TimeManager.updateTargetClock), userInfo: nil, repeats: true)
    }

    func updateTargetClock() {
        target.updateClock()
    }
}
