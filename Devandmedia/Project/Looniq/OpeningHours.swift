//
//  OpeningHours.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import Foundation

struct OpeningHours {
    var opening: Date
    var closing: Date

    var openingString: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: opening)
    }

    var closingString: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: closing)
    }

    var isOpen: Bool {
        let now = Date()
        if now > opening && now < closing {
            return true
        }
        return false
    }

    var isClosedToday: Bool {
        return opening == closing
    }
}

extension OpeningHours {
    init(open: Date, close: Date) {
        let calendar = Calendar(identifier: .gregorian)
        let todayComps = calendar.dateComponents([.day, .month, .year], from: Date())
        var openComps = calendar.dateComponents([.hour, .minute, .second], from: open)

        openComps.day = todayComps.day
        openComps.month = todayComps.month
        openComps.year = todayComps.year

        var closeComps = calendar.dateComponents([.hour, .minute, .second], from: close)
        closeComps.day = todayComps.day
        closeComps.month = todayComps.month
        closeComps.year = todayComps.year

        if openComps.hour! > closeComps.hour! {
            closeComps.day! += 1
        }

        opening = calendar.date(from: openComps)!
        closing = calendar.date(from: closeComps)!
    }
}
