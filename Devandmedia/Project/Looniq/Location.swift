//
//  Location.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import CoreLocation

protocol FutureEventDownloadDelegate {
    func futureEventsDownloaded(_ events: [Event])
}

struct Location: CustomStringConvertible {
    var id: Int
    var name: String
    var address: String?
    var latitude: CLLocationDegrees
    var longitude: CLLocationDegrees
    var mail: String?
    var number: String?
    var facebook: String?
    var dresscode: String?
    var website: String?
    var info: String?
    var category: String?
    var drinks: UIImage?
    var openingHours: OpeningHours?
    var eventPriceClass: Int?

    var futureDelegate: FutureEventDownloadDelegate?

    var description: String {
        return "\(id) - \(name)"
    }

    func downloadFutureEvents() {
        let jsonFileUrl = URL(string: looniqUrl + "eventsForLocation.php")!
        let postMessage = "futureEventRequestFromApp=YES&locationId=\(self.id)"

        var request = URLRequest(url: jsonFileUrl)
        request.httpBody = postMessage.data(using: .utf8)
        request.httpMethod = "POST"

        let session = URLSession(configuration: BasicAuth.config)
        let task = session.dataTask(with: request) {
            data, response, error in

            if error != nil {
                print(error?.localizedDescription ?? "")
                return
            }

            guard let data = data else { return }
            var events = [Event]()
            var jsonArray: [[String: AnyObject]]

            do {
                guard let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [[String: AnyObject]] else { return }
                jsonArray = dict
            } catch let jsonerror {
                print("json error: \(jsonerror)")
                return
            }

            events = self.getEventsAfterToday(from: jsonArray)

            print(events)

            self.futureDelegate?.futureEventsDownloaded(events)
        }
        task.resume()
    }

    func getEventsAfterToday(from dict: [[String: AnyObject]]) -> [Event] {
        var events = [Event]()

        for jsonEvent in dict {
            let event = Event(withDictionary: jsonEvent)

            if event.date > Date() {
                events.append(event)
            }
        }

        return events
    }
}

extension Location {
    init(withDictionary dictionary: [String: AnyObject]) {
        id = Int(dictionary["id"] as! String)!
        if let name = dictionary["name"] as? String {
            self.name = name
        } else {
            self.name = ""
        }
        address = dictionary["address"] as? String
        if let latString = dictionary["latitude"] as? String, let latitude = Double(latString) {
            self.latitude = latitude
        } else {
            self.latitude = 0.0
        }
        if let longString = dictionary["longitude"] as? String, let longitude = Double(longString) {
            self.longitude = longitude
        } else {
            self.longitude = 0.0
        }
        mail = dictionary["mail"] as? String
        number = dictionary["phonenumber"] as? String
        facebook = dictionary["facebook"] as? String
        dresscode = dictionary["dresscode"] as? String
        website = dictionary["website"] as? String
        info = dictionary["info"] as? String
        category = dictionary["category"] as? String
        guard let hours = dictionary["openingHours"] as? [Dictionary<String, AnyObject>] else { return }

        openingHours = nil
        for hourItem in hours {
            let calendar = Calendar(identifier: .gregorian)
            // 1...Sunday to 7...Saturday
            let weekDay = calendar.component(.weekday, from: Date())
            // Check if dayOfWeek is current day
            guard let weekDayString = hourItem["dayOfWeek"] as? String, weekDay == Int(weekDayString) else { continue }

            guard let openTime = hourItem["openTime"] as? String, let closeTime = hourItem["closeTime"] as? String else { continue }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm:ss"
            guard let openDate = dateFormatter.date(from: openTime), let closeDate = dateFormatter.date(from: closeTime) else { continue }
            openingHours = OpeningHours(open: openDate, close: closeDate)
        }
    }

    init() {
        id = 0
        name = ""
        address = nil
        latitude = 0.0
        longitude = 0.0
        mail = nil
        number = nil
        facebook = nil
        dresscode = nil
        website = nil
        info = nil
        category = nil
        openingHours = nil
    }
}
