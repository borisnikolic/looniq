//
//  URLHandler.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import FBSDKCoreKit

struct URLHandler {
    func handleOpening(url: URL, app: UIApplication, sourceApplication: String?, annotation: Any?) -> Bool {
        if url.scheme == "looniq" {
            return handleDeepLinking(for: url)
        } else {
            return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: sourceApplication, annotation: annotation)
        }
    }

    func handleDeepLinking(for url: URL) -> Bool {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return false }
        if url.host == "event" {
            let homeView = HomeViewController()
            guard let query = url.query, let id = Int(query) else { return false }
            homeView.id = id

            appDelegate.window?.rootViewController = homeView

            return true
        } else if url.host == "home" {
            let vc = HomeViewController()
            appDelegate.window?.rootViewController = vc

            return true
        }
        
        return false
    }
}
