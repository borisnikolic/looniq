//
//  TableViewCells.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import UIKit

protocol Reusable: class {
    static var reuseIdentifier: String { get }
}

extension Reusable {
    static var reuseIdentifier: String {
        return String(describing: Self.self)
    }
}

extension UITableView {
    func registerReusableCell<T: UITableViewCell>(_: T.Type) where T: Reusable {
        self.register(T.self, forCellReuseIdentifier: T.reuseIdentifier)
    }

    func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T where T: Reusable {
        return self.dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as! T
    }
}

class BaseCell: UITableViewCell, Reusable {
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        textLabel?.textAlignment = .center
        textLabel?.textColor = .white
        backgroundColor = .clear
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupSelecting() {
        let backView = UIView()
        backView.backgroundColor = UIColor(red: 0.86, green: 0.86, blue: 0.86, alpha: 0.7)
        selectedBackgroundView = backView
    }
}

class DateCell: BaseCell {
    func fill(dateLabel: String) {
        textLabel?.text = dateLabel

        textLabel?.font = textLabel?.font.withSize(16.0)
        accessibilityIdentifier = "DateCell"
        selectionStyle = .none
    }
}

class InfoCell: BaseCell {
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func fill(title: String, text: String?) {
        let view = UIView()
        view.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.3)
        backgroundView = view
        textLabel?.text = title
        detailTextLabel?.textColor = .white
        detailTextLabel?.text = text
        detailTextLabel?.numberOfLines = 0
        detailTextLabel?.lineBreakMode = .byWordWrapping
        selectionStyle = .none
        accessibilityIdentifier = "InfoCell"
    }
}

class AddEventCell: BaseCell {
    func fill() {
        setupSelecting()
        backgroundView = nil
        textLabel?.text = "Füge ein Event hinzu…".localized
        textLabel?.font = textLabel?.font.withSize(12.0)
        accessibilityIdentifier = "AddEventCell"
    }
}

class EventCell: BaseCell {
    var event = Event()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func fill(event: Event) {
        commonFill(for: event)

        let detailText = "\(event.location.name)"
        detailTextLabel?.text = detailText
    }

    func fillForFuture(event: Event) {
        commonFill(for: event)

        let dateFormatter = DateFormatter()
        dateFormatter.setLocalizedDateFormatFromTemplate("MMd")
        let dateText = dateFormatter.string(from: event.date)
        let detailText = "\(dateText) | \(event.time)"
        event.win = false
        detailTextLabel?.text = detailText
    }

    func commonFill(for event: Event) {
        setupSelecting()
        self.event = event

        accessoryType = .disclosureIndicator

        set(image: event.image)

        textLabel?.textColor = .white
        detailTextLabel?.textColor = .white

        textLabel?.text = event.name

        accessibilityIdentifier = event.name
    }

    func set(image: UIImage?) {
        let img = image ?? UIImage(named: "image")!
        let imageView = UIImageView()
        if let gradientImage = img.imageWithLeftToRightGradientOverlay() {
            imageView.image = gradientImage
        }
        imageView.frame = frame
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true

        if event.ad {
            imageView.layer.cornerRadius = 0.0
            imageView.layer.borderWidth = 2.0
            imageView.layer.borderColor = UIColor.white.cgColor
        }

        backgroundView = imageView
    }
}

class LocationCell: BaseCell {
    var location = Location()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func fill(location: Location) {
        setupSelecting()
        self.location = location

        accessoryType = .disclosureIndicator

        textLabel?.textColor = .white
        detailTextLabel?.textColor = .white

        textLabel?.text = location.name

        accessibilityIdentifier = location.name
    }
}

extension UIImage {
    func imageWithLeftToRightGradientOverlay() -> UIImage? {
        return imageWithGradient(from: CGPoint(x: self.size.width, y: self.size.height/2), to: CGPoint(x: 0, y: self.size.height / 2))
    }

    func imageWithBottomToTopGradientOverlay() -> UIImage? {
        return imageWithGradient(from: CGPoint(x: self.size.width/2, y: 0), to: CGPoint(x: self.size.width/2, y: self.size.height))
    }

    func imageWithGradient(from start: CGPoint, to end: CGPoint) -> UIImage? {
        defer {
            UIGraphicsEndImageContext()
        }
        UIGraphicsBeginImageContext(self.size)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }

        self.draw(at: CGPoint(x: 0, y: 0))

        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let locations: [CGFloat] = [0.0, 1.0]
        let bottom = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7).cgColor
        let top = UIColor(red: 0, green: 0, blue: 0, alpha: 0).cgColor

        guard let gradient = CGGradient(colorsSpace: colorSpace, colors: [top, bottom] as CFArray, locations: locations) else { return nil }
        context.drawLinearGradient(gradient, start: start, end: end, options: CGGradientDrawingOptions.init(rawValue: 0))

        guard let finalImage = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        return finalImage
    }
}
