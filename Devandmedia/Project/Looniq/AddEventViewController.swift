//
//  AddEventViewController.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import UIKit
import JVFloatLabeledTextField

@IBDesignable
class FormTextField: JVFloatLabeledTextField {
    @IBInspectable var inset: CGFloat = 0

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return super.textRect(forBounds: bounds).insetBy(dx: inset, dy: inset)
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
}

@IBDesignable
class AutocompleteFormTextField: MLPAutoCompleteTextField {
    @IBInspectable var inset: CGFloat = 0

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return super.textRect(forBounds: bounds).insetBy(dx: inset, dy: inset)
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
}

class AddEventViewController: FormViewController {
    @IBOutlet weak var dateTextField: JVFloatLabeledTextField!
    @IBOutlet weak var eventTextField: MLPAutoCompleteTextField!
    @IBOutlet weak var locationTextField: MLPAutoCompleteTextField!
    @IBOutlet weak var timeTextField: JVFloatLabeledTextField!
    @IBOutlet weak var priceTextField: JVFloatLabeledTextField!
    @IBOutlet weak var descriptionTextView: JVFloatLabeledTextView!
    @IBOutlet weak var contactNameTextField: JVFloatLabeledTextField!
    @IBOutlet weak var contactEmailTextField: JVFloatLabeledTextField!
    @IBOutlet weak var durationTextField: JVFloatLabeledTextField!
    @IBOutlet weak var genreTextField: JVFloatLabeledTextField!
    @IBOutlet weak var ageRatingTextField: JVFloatLabeledTextField!

    var durationPicker = UIPickerView()
    var genrePicker = UIPickerView()

    var eventManager = EventManager()

    var locations: [String] = []
    var modifiedlocations: [String] = []

    var eventNames: [String] = []
    var modifiedeventNames: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Neues Event".localized

        eventManager.setupEvents()
        eventManager.downloadLocations()
        eventManager.locationDownloadDelegate = self
        eventManager.delegate = self

        locationTextField.autoCompleteTableBorderColor = .gray
        locationTextField.autoCompleteTableCellTextColor = .gray
        locationTextField.autoCompleteTableOriginOffset = CGSize(width: 0, height: 0)
        locationTextField.autoCompleteTableCornerRadius = 0.0
        locationTextField.autoCompleteDataSource = self
        locationTextField.maximumEditDistance = 4
        locationTextField.addTarget(self, action: #selector(locationTextFieldDidChange(_:)), for: .editingChanged)

        eventTextField.autoCompleteTableBorderColor = .gray
        eventTextField.autoCompleteTableCellTextColor = .gray
        eventTextField.autoCompleteTableOriginOffset = CGSize(width: 0, height: 0)
        eventTextField.autoCompleteTableCornerRadius = 0.0
        eventTextField.autoCompleteDataSource = self
        eventTextField.maximumEditDistance = 2
        eventTextField.addTarget(self, action: #selector(eventNameTextFieldDidChange(_:)), for: .editingChanged)

        descriptionTextView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        descriptionTextView.textContainerInset = UIEdgeInsets(top: 0, left: 15, bottom: 15, right: 15)
        descriptionTextView.placeholder = "Beschreibung".localized

        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .dateAndTime
        datePicker.minuteInterval = 30
        datePicker.addTarget(self, action: #selector(AddEventViewController.dateChanged(_:)), for: .valueChanged)
        dateTextField.inputView = datePicker
        timeTextField.inputView = datePicker
        dateTextField.delegate = self
        timeTextField.delegate = self

        durationPicker.delegate = self
        durationPicker.dataSource = self
        durationTextField.inputView = durationPicker
        durationTextField.delegate = self

        genrePicker.delegate = self
        genrePicker.dataSource = self
        genreTextField.inputView = genrePicker
        genreTextField.delegate = self

        let sendButton = UIBarButtonItem(title: "Senden".localized, style: .done, target: self, action: #selector(AddEventViewController.sendButtonTapped(_:)))
        sendButton.tintColor = #colorLiteral(red: 0.4117647059, green: 0.5843137255, blue: 0.8509803922, alpha: 1)
        navigationItem.rightBarButtonItem = sendButton
    }

    func sendButtonTapped(_ sender: AnyObject) {
        let url = URL(string: looniqUrl + "insert.php")
        guard var bodyData = extractTextViewValues() else { return }
        print(bodyData)
        bodyData += "&emailRequestFromApp=YES"
        sendPostRequest(to: url!, bodyData: bodyData, successText: "Vielen Dank für deine Anfrage! Wir werden sie so bald wie möglich bearbeiten!".localized)
    }

    func extractTextViewValues() -> String? {
        let eventText = eventTextField.text!
        let locationText = locationTextField.text!
        var timeText = timeTextField.text!
        timeText += ":00"
        let priceText = priceTextField.text!
        let descriptionText = descriptionTextView.text!
        let dateText = dateTextField.text!

        let genreText = GenreFilter(string: genreTextField.text!)?.nonLocalizedDescription ?? ""
        let ageRatingText = ageRatingTextField.text!

        var endDateText = ""
        if durationTextField.text != "" && durationTextField.text != "1" {
            let duration = Double(durationTextField.text != "" ? durationTextField.text! : "1")! - 1
            let endDate = (dateTextField.inputView as? UIDatePicker)?.date.addingTimeInterval(duration * 24.0 * 60.0 * 60.0)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            endDateText = dateFormatter.string(from: endDate!)
        }

        let contactName = contactNameTextField.text!
        let contactMail = contactEmailTextField.text!

        if contactMail == "" && dateText == "" && eventText == "" && locationText == "" {
            showHUD(with: "Bitte Felder ausfüllen".localized)
            return nil
        }

        if !isValidEmail(contactMail) {
            showHUD(with: "Keine gültige Email".localized)
            return nil
        }

        if contactMail == "" || dateText == "" || eventText == "" || locationText == "" {
            showHUD(with: "Bitte Felder ausfüllen".localized)
            return nil
        }

        return "contactNameApp=\(contactName)&contactMailApp=\(contactMail)&dateApp=\(dateText)&eventApp=\(eventText)&locationApp=\(locationText)&timeApp=\(timeText)&priceApp=\(priceText)&descriptionApp=\(descriptionText)&priceClassApp=0&genreApp=\(genreText)&ageRatingApp=\(ageRatingText)&endDateApp=\(endDateText)".encodeURIComponent()
    }

    func dateChanged(_ sender: AnyObject) {
        guard let datePicker = sender as? UIDatePicker else { return }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"

        dateTextField.text = dateFormatter.string(from: datePicker.date)

        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HH:mm"

        timeTextField.text = timeFormatter.string(from: datePicker.date)
    }

    func locationTextFieldDidChange(_ textField: UITextField) {
        guard let text = textField.text else { return }
        modifiedlocations = locations
        modifiedlocations.append(text)
        locationTextField.autoCompleteTableView.reloadData()
    }

    func eventNameTextFieldDidChange(_ textField: UITextField) {
        guard let text = textField.text else { return }
        modifiedeventNames = eventNames
        modifiedeventNames.append(text)
        eventTextField.autoCompleteTableView.reloadData()
    }
}

extension AddEventViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == durationPicker {
            return 14
        } else if pickerView == genrePicker {
            return GenreFilter.values.count-1
        }
        return 0
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == durationPicker {
            return "\(row+1)"
        } else if pickerView == genrePicker {
            return GenreFilter(rawValue: row+1)?.description
        }
        return ""
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == durationPicker {
            durationTextField.text = "\(row+1)"
        } else if pickerView == genrePicker {
            genreTextField.text = GenreFilter(rawValue: row+1)?.description
        }
    }
}

extension AddEventViewController: MLPAutoCompleteTextFieldDataSource {
    func autoCompleteTextField(_ textField: MLPAutoCompleteTextField!, possibleCompletionsFor string: String!) -> [Any]! {
        if textField == locationTextField {
            return modifiedlocations
        } else if textField == eventTextField {
            return modifiedeventNames
        }
        return []
    }
}

extension AddEventViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
}

extension AddEventViewController: EventDownloadDelegate {
    func eventsDownloaded(_ events: [Event]) {
        self.eventNames = eventManager.allPastAndFutureEvents.map { event -> String in
            return event.name
            }.removeDuplicates()
    }
}

extension AddEventViewController: LocationDownloadDelegate {
    func locationsDownloaded(_ locations: [Location]) {
        self.locations = locations.map { location -> String in
            return location.name
        }
    }
}

extension Array where Element: Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()

        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }

        return result
    }
}
