//
//  FormViewController.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import UIKit
import MBProgressHUD

class FormViewController: BaseViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var messageLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        scrollView.bounds = self.view.bounds
        scrollView.delegate = self
        scrollView.backgroundColor = UIColor.clear
        self.addBackgroundImageToView()
        self.view.sendSubview(toBack: headerImage)
        scrollView.contentSize = self.view.bounds.size

        NotificationCenter.default.addObserver(self, selector: #selector(FormViewController.keyboardDidShow(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FormViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    func findActiveResponderFrame(_ view: UIView) -> UIView? {
        if view.isFirstResponder {
            return view
        } else {
            for sub in view.subviews {
                if let found = findActiveResponderFrame(sub) {
                    return found
                }
            }
        }
        return nil
    }

    func keyboardDidShow(_ notification: Notification) {
        guard let keyboardSize = (notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue else { return }
        var contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
        scrollView.contentInset = contentInsets

        // Show hidden text field
        var frame = view.frame
        let navBarHeight = self.navigationController?.navigationBar.frame.height ?? 44
        frame.size.height -= keyboardSize.height - navBarHeight
        frame.origin.y -= keyboardSize.height / 2 - navBarHeight

        guard let activeField = findActiveResponderFrame(scrollView) else { return }
        let middle = CGPoint(x: activeField.frame.midX, y: activeField.frame.midY)
        if activeField is MLPAutoCompleteTextField {
            contentInsets.bottom += 100
            scrollView.contentInset = contentInsets
        }

        if !frame.contains(middle) {
            let scrollPoint = CGPoint(x: 0, y: activeField.frame.origin.y - keyboardSize.height/2)
            scrollView.setContentOffset(scrollPoint, animated: true)
        }
    }

    func keyboardWillHide(_ notification: Notification) {
        scrollView.contentInset = UIEdgeInsets.zero
    }

    func isValidEmail(_ testStr: String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"

        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }

    func sendPostRequest(to url: URL, bodyData: String, successText: String) {
        let request = NSMutableURLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 2.0)
        request.httpMethod = "POST"

        let requestBodyData = bodyData as NSString
        request.httpBody = bodyData.data(using: String.Encoding.utf8)

        // Set content length
        URLProtocol.setProperty(requestBodyData.length, forKey: "Content-Length", in: request)

        // Basic auth
        let loginString = NSString(format: "Looniq:password")
        let loginData = loginString.data(using: String.Encoding.utf8.rawValue)!
        let base64LoginString = loginData.base64EncodedString(options: .lineLength64Characters)
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")

        var response: URLResponse?
        var urlData: Data?

        do {
            urlData = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response)
        } catch {
            urlData = nil
            print(error)
        }

        if urlData != nil {
            guard let res = response as? HTTPURLResponse else {
                showHUD(with: "Fehler aufgetreten".localized)
                return
            }

            print("Response code: %ld", res.statusCode)

            if res.statusCode >= 200 && res.statusCode < 300 {
                print("Valid status code.")
                showHUD(with: successText)
            }
        }
    }

    func showHUD(with text: String) {
        let progressHUD = MBProgressHUD.showAdded(to: view, animated: true)
        progressHUD.mode = .text
        progressHUD.detailsLabel.text = text
        progressHUD.isUserInteractionEnabled = false
        progressHUD.hide(animated: true, afterDelay: 3.0)
    }
}
