//
//  BasicAuthConfiguration.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import Foundation

let looniqUrl = "https://host10.ssl-net.net/looniq_at/Looniq_App_Protected/"

struct BasicAuth {
    static var config: URLSessionConfiguration {
        let defaultConfig = URLSessionConfiguration.default
        let userPasswordString = "Looniq:password"
        let userPasswordData = userPasswordString.data(using: .utf8)
        let base64EncodedCredential = userPasswordData!.base64EncodedString()
        let authString = "Basic \(base64EncodedCredential)"
        defaultConfig.httpAdditionalHeaders = ["Authorization": authString]

        defaultConfig.urlCache = nil
        defaultConfig.urlCredentialStorage = nil
        defaultConfig.httpCookieStorage = nil

        return defaultConfig
    }
}
