//
//  EventFilter.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import UIKit

enum DistanceFilter: Double {
    case m200 = 200
    case m500 = 500
    case km2 = 2000
    case km5 = 5000
    case km10 = 10000

    static let values: [DistanceFilter] = [.m200, .m500, .km2, .km5, .km10]

    init?(string: String) {
        switch string {
        case "200m": self = .m200
        case "500m": self = .m500
        case "2km": self = .km2
        case "5km": self = .km5
        case "10km": self = .km10
        default: return nil
        }
    }
}

extension DistanceFilter: CustomStringConvertible {
    var description: String {
        switch self {
        case .m200: return "200m"
        case .m500: return "500m"
        case .km2: return "2km"
        case .km5: return "5km"
        case .km10: return "10km"
        }
    }
}

enum LocationCategoryFilter {
    case all
    case club
    case disco
    case restaurant
    case pub
    case privateevent
    case lounge
    case bar
    case other

    static let values: [LocationCategoryFilter] = [.all, .club, .disco, .lounge, .pub, .bar, .restaurant, .privateevent, .other]

    init?(string: String) {
        switch string {
        case "Alle".localized: self = .all
        case "Club".localized: self = .club
        case "Disco".localized: self = .disco
        case "Restaurant".localized: self = .restaurant
        case "Pub".localized: self = .pub
        case "Privatveranstaltung".localized: self = .privateevent
        case "Lounge".localized: self = .lounge
        case "Bar".localized: self = .bar
        case "Andere".localized: self = .other
        default: return nil
        }
    }
}

extension LocationCategoryFilter: CustomStringConvertible {
    var description: String {
        switch self {
        case .all: return "Alle".localized
        case .club: return "Club".localized
        case .disco: return "Disco".localized
        case .restaurant: return "Restaurant".localized
        case .pub: return "Pub".localized
        case .privateevent: return "Privatveranstaltung".localized
        case .lounge: return "Lounge".localized
        case .bar: return "Bar".localized
        case .other: return "Andere".localized
        }
    }
}

enum GenreFilter: Int {
    case all
    case hiphop
    case latin
    case jazz
    case drumandbass
    case oriental
    case rnb
    case rock
    case folk
    case dancehall
    case balkan
    case techno
    case pop
    case metal
    case country
    case house
    case schlager
    case classic
    case reggae
    case edm
    case oldies
    case trance
    case urban
    case hardstyle
    case ninties
    case goa
    case blues
    case other

    static let values: [GenreFilter] = [.all, .pop, .house, .edm, .drumandbass, .oriental, .rnb, .rock, .folk, .dancehall, .balkan, .techno, .hiphop, .metal, .country, .latin, .schlager, .classic, .reggae, .hiphop, .oldies, .trance, .urban, .hardstyle, .ninties, .goa, .blues, .other]

    init?(string: String) {
        switch string {
        case "Alle".localized: self = .all
        case "Hip Hop".localized: self = .hiphop
        case "Latin".localized: self = .latin
        case "Jazz".localized: self = .jazz
        case "Drum and Bass".localized: self = .drumandbass
        case "Oriental".localized: self = .oriental
        case "R'n'B".localized: self = .rnb
        case "Rock".localized: self = .rock
        case "Folk".localized: self = .folk
        case "Dancehall".localized: self = .dancehall
        case "Balkan".localized: self = .balkan
        case "Techno".localized: self = .techno
        case "Pop".localized: self = .pop
        case "Metal".localized: self = .metal
        case "Country".localized: self = .country
        case "House".localized: self = .house
        case "Schlager".localized: self = .schlager
        case "Classic".localized: self = .classic
        case "Reggae".localized: self = .reggae
        case "EDM".localized: self = .edm
        case "Oldies".localized: self = .oldies
        case "Trance".localized: self = .trance
        case "Urban".localized: self = .urban
        case "Hardstyle".localized: self = .hardstyle
        case "90's".localized: self = .ninties
        case "Goa".localized: self = .goa
        case "Blues".localized: self = .blues
        case "Andere".localized: self = .other
        default: return nil
        }
    }
}

extension GenreFilter: CustomStringConvertible {
    var description: String {
        switch self {
        case .all:          return "Alle".localized
        case .hiphop:       return "Hip Hop".localized
        case .latin:        return "Latin".localized
        case .jazz:         return "Jazz".localized
        case .drumandbass:  return "Drum and Bass".localized
        case .oriental:     return "Oriental".localized
        case .rnb:          return "R'n'B".localized
        case .rock:         return "Rock".localized
        case .folk:         return "Folk".localized
        case .dancehall:    return "Dancehall".localized
        case .balkan:       return "Balkan".localized
        case .techno:       return "Techno".localized
        case .pop:          return "Pop".localized
        case .metal:        return "Metal".localized
        case .country:      return "Country".localized
        case .house:        return "House".localized
        case .schlager:     return "Schlager".localized
        case .classic:      return "Classic".localized
        case .reggae:       return "Reggae".localized
        case .edm:          return "EDM".localized
        case .oldies:       return "Oldies".localized
        case .trance:       return "Trance".localized
        case .urban:        return "Urban".localized
        case .hardstyle:    return "Hardstyle".localized
        case .ninties:      return "90's".localized
        case .goa:          return "Goa".localized
        case .blues:        return "Blues".localized
        case .other:        return "Andere".localized
        }
    }

    var nonLocalizedDescription: String {
        switch self {
        case .hiphop:       return "Hip Hop"
        case .latin:        return "Latin"
        case .jazz:         return "Jazz"
        case .drumandbass:  return "Drum and Bass"
        case .oriental:     return "Oriental"
        case .rnb:          return "R'n'B"
        case .rock:         return "Rock"
        case .folk:         return "Folk"
        case .dancehall:    return "Dancehall"
        case .balkan:       return "Balkan"
        case .techno:       return "Techno"
        case .pop:          return "Pop"
        case .metal:        return "Metal"
        case .country:      return "Country"
        case .house:        return "House"
        case .schlager:     return "Schlager"
        case .classic:      return "Classic"
        case .reggae:       return "Reggae"
        case .edm:          return "EDM"
        case .oldies:       return "Oldies"
        case .trance:       return "Trance"
        case .urban:        return "Urban"
        case .hardstyle:    return "Hardstyle"
        case .ninties:      return "90's"
        case .goa:          return "Goa"
        case .blues:        return "Blues"
        default:            return ""
        }
    }
}

enum PriceFilter {
    case all
    case cheap
    case medium
    case expensive

    static let values: [PriceFilter] = [.all, .cheap, .medium, .expensive]

    init?(string: String) {
        switch string {
        case "Alle".localized: self = .all
        case "0-5": self = .cheap
        case "6-10": self = .medium
        case "11-∞": self = .expensive
        default: return nil
        }
    }
}

extension PriceFilter: CustomStringConvertible {
    var description: String {
        switch self {
        case .all: return "Alle".localized
        case .cheap: return "0-5"
        case .medium: return "6-10"
        case .expensive: return "11-∞"
        }
    }
}

struct FilterOption {
    var distance: DistanceFilter = .km10
    var category: LocationCategoryFilter = .all
    var genre: GenreFilter = .all
    var price: PriceFilter = .all
    var nowOpen: Bool = false

    static let values = ["Distanz".localized, "Genre".localized, "Kategorie".localized, "Preis".localized, "Jetzt geöffnet".localized]

    mutating func reset() {
        distance = .km10
        category = .all
        genre = .all
        price = .all
        nowOpen = false
    }
}

protocol EventFilterDelegate {
    func eventFilterChanged(to option: FilterOption)
}

class EventFilter {
    static let sharedInstance = EventFilter()
    var sortBy = FilterOption()
    var delegate: EventFilterDelegate?

    func sort(events: [Event]) -> [Event] {
        var sortedEvents = events

        // Filter distance
        sortedEvents = sortedEvents.filter { event in
            if let distance = event.distance?.meter {
                if distance <= sortBy.distance.rawValue {
                    return true
                } else {
                    return false
                }
            } else {
                return false
            }
        }

        // Filter category
        sortedEvents = sortedEvents.filter { event in
            if (event.location.category == sortBy.category.description) || (sortBy.category == .all) {
                return true
            } else {
                return false
            }
        }

        // Filter genre
        sortedEvents = sortedEvents.filter { event in
            if (event.genre == sortBy.genre.description) || (sortBy.genre == .all) {
                return true
            } else {
                return false
            }
        }

        // Filter price
        sortedEvents = sortedEvents.filter { event in
            guard let price = event.price else {
                if sortBy.price == .all {
                    return true
                } else {
                    return false
                }
            }

            switch sortBy.price {
            case .all: return true
            case .cheap: if price <= 5.0 { return true } else { return false }
            case .medium: if price > 5.0 && price <= 10.0 { return true } else { return false }
            case .expensive: if price > 10.0 { return true } else { return false }
            }
        }

        // Filter isOpen
        sortedEvents = sortedEvents.filter { event in
            if sortBy.nowOpen {
                if let opening = event.location.openingHours {
                    if opening.isOpen {
                        return true
                    } else {
                        return false
                    }
                } else {
                    return false
                }
            } else {
                return true
            }
        }

        return sortedEvents
    }

    func sortOptionChanged(to option: FilterOption) {
        sortBy = option
        delegate?.eventFilterChanged(to: option)
    }
}
