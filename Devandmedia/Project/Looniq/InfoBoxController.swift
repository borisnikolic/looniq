//
//  InfoBoxController.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import Foundation
import CoreLocation

protocol InfoBoxControllerDownloadDelegate {
    func infosFinishedDownloading(_ infos: [Info])
}

class InfoBoxController: NSObject {
    var infos = [Info]()
    var delegate: InfoBoxControllerDownloadDelegate?

    func downloadInfos() {
        let url = URL(string: looniqUrl + "infobox.php")!

        let session = URLSession(configuration: BasicAuth.config)
        let task = session.dataTask(with: url) {
            data, response, error in
            guard let data = data else { return }
            self.infos = [Info]()
            var jsonArray: [[String: String]]?
            do {
                jsonArray = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [[String: String]]
            } catch {
                return
            }

            guard let array = jsonArray else { return }

            for jsonInfo in array {
                let info = Info(withDictionary: jsonInfo)
                self.infos.append(info)
            }

            print(array)
        }
        task.resume()

        LocationManager.sharedManager.infoDelegate = self
        LocationManager.sharedManager.startLocationUpdates()
    }
}

extension InfoBoxController: LocationManagerDelegate {
    func didUpdateLocation(_ location: CLLocation) {
        LocationManager.sharedManager.infoDelegate = nil

        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(location) { placemarks, error in
            self.infos = self.infos.filter { info in
                if info.country != nil {
                    let placemark = placemarks?.first
                    guard let country = placemark?.isoCountryCode else { return false }
                    print(country)
                    if info.country == country {
                        return true
                    } else {
                        return false
                    }
                } else if let lat = info.latitude, let long = info.longitude, let range = info.range {
                    guard let lat = CLLocationDegrees(lat) else { return false }
                    guard let long = CLLocationDegrees(long) else { return false }
                    let newDistance = location.distance(from: CLLocation(latitude: lat, longitude: long))
                    if newDistance / 1000 < range {
                        return true
                    } else {
                        return false
                    }
                } else {
                    return info.everywhere
                }
            }
            self.delegate?.infosFinishedDownloading(self.infos)
        }
    }
}
