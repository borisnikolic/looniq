//
//  Info.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import Foundation

struct Info {
    var title: String
    var text: String?
    var distance: Distance?
    var country: String?
    var latitude: String?
    var longitude: String?
    var range: Double?
    var everywhere: Bool

    init(withDictionary dictionary: [String: String]) {
        title = dictionary["title"]!
        text = dictionary["text"]
        distance = nil
        country = nil
        latitude = nil
        longitude = nil
        range = nil
        everywhere = true

        if let countryString = dictionary["country"] {
            country = countryString
            everywhere = false
        }
        if let latString = dictionary["latitude"] {
            latitude = latString
            everywhere = false
        }
        if let longString = dictionary["longitude"] {
            longitude = longString
            everywhere = false
        }
        if let rangeString = dictionary["range"], let range = Double(rangeString) {
            self.range = range
            everywhere = false
        }
    }
}

extension Info: Equatable {
    static func ==(lhs: Info, rhs: Info) -> Bool {
        if (lhs.title == rhs.title) && (lhs.text == rhs.text) && (lhs.country == rhs.country) && (lhs.longitude == rhs.longitude) && (lhs.latitude == rhs.latitude) {
            return true
        }
        return false
    }
}
