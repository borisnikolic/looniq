//
//  OnboardingViewController.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import Foundation
import Locksmith
import FBSDKLoginKit

class OnboardingViewController: UIViewController, FBSDKLoginButtonDelegate {
    @IBOutlet weak var facebookLoginView: UIView!
    let loginButton = FBSDKLoginButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        loginButton.readPermissions = ["public_profile", "email", "user_events"]
        loginButton.center = facebookLoginView.center
        loginButton.delegate = self
        view.addSubview(loginButton)

        profileChanged()

        FBSDKProfile.enableUpdates(onAccessTokenChange: true)
        NotificationCenter.default.addObserver(self, selector: #selector(OnboardingViewController.profileChanged), name: NSNotification.Name.FBSDKProfileDidChange, object: nil)
    }

    override func viewDidLayoutSubviews() {
        loginButton.center = facebookLoginView.center
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @IBAction func continueTapped(_ sender: AnyObject) {
        UserDefaults.standard.set(true, forKey: "facebookSplashscreenDone")
        let homeView = HomeViewController()
        present(homeView, animated: true)
        UIApplication.shared.keyWindow?.rootViewController = homeView
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if result == nil || result.isCancelled || error != nil { return }
        print("Logged in")
        continueTapped(loginButton)
    }

    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {}

    func profileChanged() {
        if FBSDKProfile.current() != nil, let name = FBSDKProfile.current().firstName, let token = FBSDKAccessToken.current() {
            do {
                let dict = ["tokenString": token.tokenString, "appID": token.appID, "userID": token.userID, "expirationDate": token.expirationDate, "refreshDate": token.refreshDate, "publishPermissions": false, "profileFirstName": name] as [String: Any]
                try Locksmith.saveData(data: dict, forUserAccount: "me")
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
}
