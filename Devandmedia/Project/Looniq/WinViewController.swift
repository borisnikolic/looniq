//
//  WinViewController.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import UIKit

class WinViewController: FormViewController {
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var birthdateTextField: UITextField!
    @IBOutlet weak var plzTextField: UITextField!
    @IBOutlet weak var teilnahmeSwitch: UISwitch!
    var eventId: Int?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Gewinne".localized

        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date

        let today = Date()
        let cal = Calendar.current
        var dateOnlyToday = cal.dateComponents([.year, .month, .day], from: today)
        dateOnlyToday.year = dateOnlyToday.year! - 18
        let yearsago = cal.date(from: dateOnlyToday)

        datePicker.maximumDate = yearsago
        datePicker.addTarget(self, action: #selector(WinViewController.dateChanged(_:)), for: .valueChanged)
        birthdateTextField.inputView = datePicker

        let sendButton = UIBarButtonItem(title: "Senden".localized, style: .done, target: self, action: #selector(WinViewController.sendButtonTapped(_:)))
        sendButton.tintColor = #colorLiteral(red: 0.4117647059, green: 0.5843137255, blue: 0.8509803922, alpha: 1)
        navigationItem.rightBarButtonItem = sendButton
    }

    func sendButtonTapped(_ sender: AnyObject) {
        let url = URL(string: looniqUrl + "win.php")
        guard var bodyData = extractTextViewValues() else { return }
        bodyData += "&winRequestFromApp=YES"
        sendPostRequest(to: url!, bodyData: bodyData, successText: "Danke für deine Teilnahme!".localized)
    }

    func extractTextViewValues() -> String? {
        let nameText = nameTextField.text!
        let birthdateText = birthdateTextField.text!
        let emailText = emailTextField.text!
        let numberText = numberTextField.text!
        let plzText = plzTextField.text!
        let accepted = teilnahmeSwitch.isOn

        if !accepted {
            //showHUD(with: "Bitte akzeptieren".localized)
            //return nil
        }

        if numberText == "" && emailText == "" {
            showHUD(with: "Email oder Telefonnummer eingeben".localized)
            return nil
        }

        if numberText == "" && !isValidEmail(emailText) {
            showHUD(with: "Keine gültige Email".localized)
            return nil
        }

        return "eventId=\(eventId!)&name=\(nameText)&mail=\(emailText)&birthdate=\(birthdateText)&number=\(numberText)&plz=\(plzText)".encodeURIComponent()
    }

    func dateChanged(_ sender: AnyObject) {
        guard let datePicker = sender as? UIDatePicker else { return }
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"

        birthdateTextField.text = formatter.string(from: datePicker.date)
    }
}
