//
//  LocationDetailViewController.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import Foundation
import MapKit
import SafariServices
import FBSDKCoreKit
import FBSDKShareKit

class LocationDetailViewController: BaseViewController, FutureEventDownloadDelegate, EventImageDownloadDelegate {
    var location: Location

    var internalCounter = 0
    var scrollToImages = false

    @IBOutlet weak var likeView: UIView!

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var openingHoursLabel: UILabel!
    @IBOutlet weak var websiteTextView: UITextView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var priceClassLabel: UILabel!

    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var drinksButton: UIButton!

    @IBOutlet weak var addressSmalllabel: UILabel!
    @IBOutlet weak var openSmallLabel: UILabel!
    @IBOutlet weak var websiteSmallLabel: UILabel!
    @IBOutlet weak var priceClassSmallLabel: UILabel!

    @IBOutlet weak var attendeesLabel: UILabel!
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var looniqAttendeesLabel: UILabel!

    var locationImages: [UIImage] = []
    @IBOutlet weak var imageCollectionView: UICollectionView!

    var headerImages: [UIImage] = []
    @IBOutlet weak var headerScrollView: UIScrollView!
    var pageControl = UIPageControl()

    var futureEvents: [Event] = []
    @IBOutlet weak var futureEventsView: UIView!
    @IBOutlet weak var futureCollectionView: UICollectionView!

    @IBOutlet weak var detailMapView: MKMapView!
    let mapView = MapUIView()

    init(with location: Location) {
        self.location = location
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        location = Location()
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        scrollView.bounds = view.bounds
        scrollView.delegate = self
        scrollView.backgroundColor = UIColor.clear
        addBackgroundImageToView()
        view.sendSubview(toBack: headerImage)
        scrollView.contentSize = view.bounds.size

        location.futureDelegate = self
        location.downloadFutureEvents()

        if location.facebook != nil && location.facebook != "" {
            let like = FBSDKLikeControl()
            like.objectID = "https://www.facebook.com/\(location.facebook!)"
            like.isSoundEnabled = false
            like.foregroundColor = #colorLiteral(red: 0.4117647059, green: 0.5843137255, blue: 0.8509803922, alpha: 1)
            likeView.addSubview(like)
        }

        drinksButton.isHidden = true

        downloadLocationImages()
        downloadLocationHeaderImages()
        downloadDrinksImage()

        if UserDefaults.standard.bool(forKey: "InternalPage\(self.location.id)") {
            self.likeButton.setTitle("Gefällt dir".localized, for: .normal)
            self.likeButton.isUserInteractionEnabled = false
        }
        sendInternalRequest(method: "GET")

        imageCollectionView.register(LocationImageCell.self, forCellWithReuseIdentifier: "LocationImageCell")
        imageCollectionView.delegate = self
        imageCollectionView.dataSource = self

        attendeesLabel.text = ""
        looniqAttendeesLabel.text = ""

        futureCollectionView.register(FutureEventCell.self, forCellWithReuseIdentifier: "FutureEventCell")
        futureCollectionView.delegate = self
        futureCollectionView.dataSource = self

        locationLabel.text = location.name

        infoLabel.numberOfLines = 0
        infoLabel.lineBreakMode = .byWordWrapping
        if location.info == "" || location.info == nil {
            infoLabel.font = UIFont.italicSystemFont(ofSize: infoLabel.font.pointSize)
            infoLabel.text = "Derzeit sind uns keine Infos bekannt.".localized
        } else {
            infoLabel.text = location.info
        }

        locationLabel.text = location.name
        addressLabel.text = location.address

        if addressLabel.text == "" || addressLabel.text == nil {
            addressSmalllabel.text = ""
        }

        websiteTextView.delegate = self
        websiteTextView.text = location.website
        if websiteTextView.text == "" || websiteTextView.text == nil {
            websiteSmallLabel.text = ""
        }

        if let hours = location.openingHours {
            openingHoursLabel.text = "\(hours.openingString) - \(hours.closingString)"

            if hours.isOpen {
                openingHoursLabel.textColor = UIColor.green
            } else {
                if hours.isClosedToday {
                    openingHoursLabel.text = "Heute geschlossen".localized
                }
                openingHoursLabel.textColor = UIColor.red
            }
        } else {
            openSmallLabel.text = ""
            openingHoursLabel.text = ""
        }

        var priceClassText = ""
        if location.eventPriceClass == 0 || location.eventPriceClass == nil {
            priceClassLabel.isHidden = true
            priceClassSmallLabel.isHidden = true
        } else {
            for _ in 0..<location.eventPriceClass! {
                priceClassText += "€"
            }
        }
        priceClassLabel.text = priceClassText

        let shareButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(LocationDetailViewController.shareAction(_:)))
        navigationItem.rightBarButtonItem = shareButton

        title = "Location"

        detailMapView.showsUserLocation = true

        let latitude = location.latitude
        let longitude = location.longitude
        let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)

        let viewRegion = MKCoordinateRegionMakeWithDistance(coordinate, 750, 750)
        detailMapView.setRegion(viewRegion, animated: true)
        detailMapView.showsUserLocation = true

        let pin = MKPointAnnotation()
        pin.coordinate = coordinate
        pin.title = location.name

        detailMapView.addAnnotation(pin)
        detailMapView.selectAnnotation(pin, animated: true)

        mapView.location = location
        scrollView.insertSubview(mapView, aboveSubview: detailMapView)

        pageControl = UIPageControl(frame: CGRect(x: 0, y: 180, width: view.frame.width, height: 20))
        pageControl.addTarget(self, action: #selector(pageControlChanged(_:)), for: .valueChanged)
        pageControl.pageIndicatorTintColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 0.5)
        scrollView.addSubview(pageControl)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        detailMapView.showsUserLocation = true
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if scrollToImages {
            if var rect = imageCollectionView.superview?.frame {
                rect.origin.y += 170
                scrollView.scrollRectToVisible(rect, animated: true)
            }
            scrollToImages = false
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        detailMapView.mapType = .hybrid
        detailMapView.mapType = .standard
        detailMapView.showsUserLocation = false
        detailMapView.delegate = nil
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        let images = headerImages.isEmpty ? [UIImage(named: "image")!] : headerImages
        setupHeaderScrollView(with: images)

        mapView.frame = detailMapView.frame
    }

    func heightForImage(image: UIImage?, width: CGFloat) -> CGFloat {
        guard let image = image else { return 0.0 }
        let oldWidth = image.size.width
        let scaleFactor = width / oldWidth

        return image.size.height * scaleFactor
    }

    func shareAction(_ sender: UIBarButtonItem) {
        let shareString = location.name + "\n\nwww.looniq.at/"

        let activityVC = UIActivityViewController(activityItems: [shareString], applicationActivities: nil)
        activityVC.excludedActivityTypes = []
        activityVC.popoverPresentationController?.barButtonItem = sender

        present(activityVC, animated: true, completion: nil)
    }

    func setupHeaderScrollView(with images: [UIImage]) {
        var xPos: CGFloat = 0
        headerScrollView.subviews.forEach { $0.removeFromSuperview() }

        for image in images {
            let imageView = UIImageView(image: image)
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            imageView.frame = CGRect(x: xPos, y: CGFloat(0), width: view.frame.width, height: 199)
            imageView.isUserInteractionEnabled = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(imageTapped(sender:)))
            imageView.addGestureRecognizer(tap)

            let gradientView = UIImageView(image: image.imageWithBottomToTopGradientOverlay())
            gradientView.contentMode = .scaleAspectFill
            gradientView.clipsToBounds = true
            gradientView.frame = CGRect(x: 0, y: CGFloat(0), width: view.frame.width, height: 199)
            gradientView.isUserInteractionEnabled = false

            imageView.addSubview(gradientView)

            headerScrollView.addSubview(imageView)
            xPos += imageView.frame.width
        }
        headerScrollView.contentSize = CGSize(width: view.frame.width*CGFloat(images.count), height: 199)
        automaticallyAdjustsScrollViewInsets = false
        headerScrollView.delegate = self

        pageControl.numberOfPages = images.count
        pageControlChanged(pageControl)
    }

    func downloadLocationImages() {
        func downloadImage(for string: String) {
            let jsonFileUrl = URL(string: looniqUrl + "uploads/\(location.name.encodeURIComponent())/\(string)")!

            let session = URLSession(configuration: BasicAuth.config)
            let task = session.downloadTask(with: jsonFileUrl) {
                url, response, error in
                guard let url = url else { return }
                var downloadedImage: UIImage?
                do {
                    try downloadedImage = UIImage(data: Data(contentsOf: url))
                } catch let error {
                    downloadedImage = nil
                    print(error.localizedDescription)
                    return
                }
                guard let image = downloadedImage else { return }
                self.locationImages.append(image)
                DispatchQueue.main.async {
                    self.imageCollectionView.reloadData()
                }
            }
            task.resume()
        }
        let jsonFileUrl = URL(string: looniqUrl + "location_images.php")!

        let postMessage = "locationImagesApp=YES&location=\(location.name.encodeURIComponent())"

        var request = URLRequest(url: jsonFileUrl)
        request.httpBody = postMessage.data(using: .utf8)
        request.httpMethod = "POST"

        let session = URLSession(configuration: BasicAuth.config)
        let task = session.dataTask(with: request) {
            data, response, error in

            if error != nil {
                print(error?.localizedDescription ?? "")
                return
            }

            guard let data = data else { return }
            var jsonArray: [String: String]

            // Parse downloaded data into JSON Dictionary
            do {
                guard let array = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: String] else { return }
                jsonArray = array
            } catch let jsonerror {
                print("json error: \(jsonerror)")
                return
            }

            let imageURLs = Array(jsonArray.values)
            print(imageURLs)

            imageURLs.forEach { string in
                downloadImage(for: string)
            }
        }
        task.resume()
    }

    func downloadLocationHeaderImages() {
        func downloadImage(for string: String) {
            let jsonFileUrl = URL(string: looniqUrl + "Locationbilder/\(location.name.encodeURIComponent())/\(string)")!

            let session = URLSession(configuration: BasicAuth.config)
            let task = session.downloadTask(with: jsonFileUrl) {
                url, response, error in
                guard let url = url else { return }
                var downloadedImage: UIImage?
                do {
                    try downloadedImage = UIImage(data: Data(contentsOf: url))
                } catch let error {
                    downloadedImage = nil
                    print(error.localizedDescription)
                    return
                }
                guard let image = downloadedImage else { return }
                self.headerImages.append(image)

                DispatchQueue.main.async {
                    self.setupHeaderScrollView(with: self.headerImages)
                }
            }
            task.resume()
        }
        let jsonFileUrl = URL(string: looniqUrl + "headers_for_location.php")!

        let postMessage = "locationImagesApp=YES&location=\(location.name.encodeURIComponent())"

        var request = URLRequest(url: jsonFileUrl)
        request.httpBody = postMessage.data(using: .utf8)
        request.httpMethod = "POST"

        let session = URLSession(configuration: BasicAuth.config)
        let task = session.dataTask(with: request) {
            data, response, error in

            if error != nil {
                print(error?.localizedDescription ?? "")
                return
            }

            guard let data = data else { return }
            var jsonArray: [String: String]

            // Parse downloaded data into JSON Dictionary
            do {
                guard let array = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: String] else { return }
                jsonArray = array
            } catch let jsonerror {
                print("json error: \(jsonerror)")
                return
            }

            let imageURLs = Array(jsonArray.values)
            print(imageURLs)

            imageURLs.forEach { string in
                downloadImage(for: string)
            }
        }
        task.resume()
    }

    func downloadDrinksImage() {
        func downloadImage(for string: String) {
            let jsonFileUrl = URL(string: looniqUrl + "Locationbilder/\(location.name.encodeURIComponent())/\(string)")!

            let session = URLSession(configuration: BasicAuth.config)
            let task = session.downloadTask(with: jsonFileUrl) {
                url, response, error in
                guard let url = url else { return }
                var downloadedImage: UIImage?
                do {
                    try downloadedImage = UIImage(data: Data(contentsOf: url))
                } catch let error {
                    downloadedImage = nil
                    print(error.localizedDescription)
                    return
                }
                guard let image = downloadedImage else { return }

                self.location.drinks = image
                DispatchQueue.main.async {
                    self.drinksButton.isHidden = false
                }
            }
            task.resume()
        }
        let jsonFileUrl = URL(string: looniqUrl + "drinksForLocation.php")!

        let postMessage = "locationDrinksImagesApp=YES&location=\(location.name.encodeURIComponent())"

        var request = URLRequest(url: jsonFileUrl)
        request.httpBody = postMessage.data(using: .utf8)
        request.httpMethod = "POST"

        let session = URLSession(configuration: BasicAuth.config)
        let task = session.dataTask(with: request) {
            data, response, error in

            if error != nil {
                print(error?.localizedDescription ?? "")
                return
            }

            guard let data = data else { return }
            var jsonArray: [String: String]

            // Parse downloaded data into JSON Dictionary
            do {
                guard let array = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: String] else { return }
                jsonArray = array
            } catch { return }

            let imageURLs = Array(jsonArray.values)
            print(imageURLs)

            if let imageURL = imageURLs.first {
                downloadImage(for: imageURL)
            }
        }
        task.resume()
    }

    func futureEventsDownloaded(_ events: [Event]) {
        futureEvents = events

        DispatchQueue.main.async {
            self.futureCollectionView.reloadData()
        }
    }

    func imageDownloaded(for event: Event) {
        futureEvents.forEach {
            if $0.name == event.name && $0.location.name == event.location.name {
                $0.image = event.image
            }
        }

        DispatchQueue.main.async {
            self.futureCollectionView.reloadData()
        }
    }

    func getCell(for event: Event) -> FutureEventCell? {
        for row in 0 ..< futureCollectionView.numberOfItems(inSection: 0) {

            let indexPath = IndexPath(row: row, section: 0)

            guard let cell = futureCollectionView.cellForItem(at: indexPath) as? FutureEventCell else { return nil }
            if cell.titleLabel?.text == event.name {
                return cell
            }
        }
        return nil
    }

    func sendInternalRequest(method: String) {
        if method == "POST" {
            incrementCounter()
        } else {
            getCounter()
        }
    }

    func sendFacebookRequest(method: String) {
        guard let facebookPage = location.facebook else { return }

        var path = ""
        if method == "GET" {
            path = "\(facebookPage)?fields=fan_count"
        } else {
            path = "\(facebookPage)/likes"
        }

        let request = FBSDKGraphRequest(graphPath: path, parameters: nil, httpMethod: method)
        _ = request?.start { _, response, error in
            print(response ?? "")
            print(error?.localizedDescription ?? "")
            guard let dict = response as? NSDictionary else { return }

            if method == "GET" {
                guard let likeCount = dict["fan_count"] as? Int else { return }
                DispatchQueue.main.async {
                    self.attendeesLabel.text = String.localizedStringWithFormat("Gefällt %d Facebook Usern".localized, likeCount)
                }
                // Already displayed in Like button
                /* guard let likers = dict["data"] as? [NSDictionary] else { return }

                 let userId = FBSDKAccessToken.current().userID
                 for liker in likers {
                 if (liker["id"] as! String) == userId {
                 self.likedPage = true
                 }
                 } */
            } else {
                guard (dict["success"] as? String) == "true" else { return }
            }
        }
    }

    func getCounter() {
        let jsonFileUrl = URL(string: looniqUrl + "counterForLocation.php")!
        let postMessage = "counterRequestApp=YES&locationId=\(location.id)"

        var request = URLRequest(url: jsonFileUrl)
        request.httpBody = postMessage.data(using: .utf8)
        request.httpMethod = "POST"

        let session = URLSession(configuration: BasicAuth.config)
        let task = session.dataTask(with: request) {
            data, response, error in
            if error != nil {
                print(error?.localizedDescription ?? "")
                return
            }

            guard let data = data else { return }

            guard let counterString = String(data: data, encoding: .utf8) else { return }

            guard let counter = Int(counterString) else { return }

            self.internalCounter = counter
            DispatchQueue.main.async {
                self.setLooniqUserLikeLabel(for: self.internalCounter)
            }
        }
        task.resume()
    }

    func incrementCounter() {
        guard !UserDefaults.standard.bool(forKey: "InternalPage\(location.id)") else { return }
        let jsonFileUrl = URL(string: looniqUrl + "incrementCounterForLocation.php")!
        let postMessage = "incrementRequestApp=YES&locationId=\(location.id)"

        var request = URLRequest(url: jsonFileUrl)
        request.httpBody = postMessage.data(using: .utf8)
        request.httpMethod = "POST"

        let session = URLSession(configuration: BasicAuth.config)
        let task = session.dataTask(with: request) {
            data, response, error in
            if error != nil {
                print(error?.localizedDescription ?? "")
                return
            }
            guard let data = data else { return }

            self.internalCounter = Int(String(data: data, encoding: .utf8)!)!
            DispatchQueue.main.async {
                self.setLooniqUserLikeLabel(for: self.internalCounter)

                UserDefaults.standard.set(true, forKey: "InternalPage\(self.location.id)")
                self.likeButton.setTitle("Gefällt dir".localized, for: .normal)
                self.likeButton.isUserInteractionEnabled = false
            }
        }
        task.resume()
    }

    func setLooniqUserLikeLabel(for count: Int) {
        guard count > 0 else { return }
        if count == 1 {
            self.looniqAttendeesLabel.text = String.localizedStringWithFormat("%d Looniq User gefällt das.".localized, self.internalCounter)
        } else {
            self.looniqAttendeesLabel.text = String.localizedStringWithFormat("%d Looniq Usern gefällt das.".localized, self.internalCounter)
        }
    }

    @IBAction func websiteButtonTapped(_ sender: AnyObject) {
        guard let website = location.website, let url = URL(string: website) else { return }
        openWebView(for: url)
    }

    @IBAction func likeButtonTapped(_ sender: AnyObject) {
        sendInternalRequest(method: "POST")
    }

    @IBAction func facebookButtonTapped(_ sender: AnyObject) {
        guard let facebook = location.facebook, let url = URL(string: facebook) else { return }
        openWebView(for: url)
    }

    func openWebView(for url: URL) {
        if #available(iOS 9.0, *) {
            let safari = SFSafariViewController(url: url)
            if #available(iOS 10.0, *) {
                safari.preferredBarTintColor = #colorLiteral(red: 0.1691845655, green: 0.1450073421, blue: 0.1447746456, alpha: 1)
            }
            safari.view.tintColor = #colorLiteral(red: 0.4117647059, green: 0.5843137255, blue: 0.8509803922, alpha: 1)
            safari.delegate = self
            present(safari, animated: true, completion: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }

    func imageTapped(sender: UITapGestureRecognizer) {
        guard let image = (sender.view as? UIImageView)?.image else { return }
        let imageViewController = ImageViewController(with: image)
        navigationController?.pushViewController(imageViewController, animated: true)
    }

    func pageControlChanged(_ sender: UIPageControl) {
        var frame = headerScrollView.frame
        frame.origin.x = frame.size.width * CGFloat(sender.currentPage)
        frame.origin.y = 0
        headerScrollView.scrollRectToVisible(frame, animated: true)
    }

    @IBAction func drinksButtonTapped(_ sender: Any) {
        guard let image = location.drinks else { return }
        let imageViewController = ImageViewController(with: image)
        navigationController?.pushViewController(imageViewController, animated: true)
    }

    // MARK: - UIScrollViewDelegate

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard scrollView == headerScrollView else { return }
        let offset = scrollView.contentOffset.x
        let width = scrollView.bounds.size.width
        pageControl.currentPage = Int(offset/width)
    }
}

extension LocationDetailViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == imageCollectionView {
            return locationImages.count
        } else if collectionView == futureCollectionView {
            return futureEvents.count
        } else {
            return 0
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == imageCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LocationImageCell", for: indexPath) as! LocationImageCell

            cell.imageView.image = locationImages[indexPath.row]

            return cell
        } else if collectionView == futureCollectionView {
            let eventCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FutureEventCell", for: indexPath) as! FutureEventCell

            let index = indexPath.row

            guard index < futureEvents.count else { return eventCell }
            let event = futureEvents[index]

            let dateFormatter = DateFormatter()
            dateFormatter.setLocalizedDateFormatFromTemplate("MMd")
            let dateString = dateFormatter.string(from: event.date)
            eventCell.titleLabel?.text = "\(event.name)\n\(dateString) | \(event.time)"
            let image = event.image ?? UIImage(named: "image")!
            eventCell.imageView.image = image.imageWithLeftToRightGradientOverlay()

            return eventCell
        } else {
            return UICollectionViewCell()
        }
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == futureCollectionView {
            futureEvents.forEach {
                $0.delegate = self
                $0.downloadImage()
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == imageCollectionView {
            guard let image = (collectionView.cellForItem(at: indexPath) as? LocationImageCell)?.imageView.image else { return }
            let imageViewController = ImageViewController(with: image)
            navigationController?.pushViewController(imageViewController, animated: true)
        } else if collectionView == futureCollectionView {
            if futureEvents.count == 0 {
                return
            }

            guard indexPath.row < futureEvents.count else { return }
            let event = futureEvents[indexPath.row]

            let detailView = UIStoryboard(name: "Detail", bundle: nil).instantiateViewController(withIdentifier: "Detail") as! DetailViewController
            detailView.details = event
            navigationController?.pushViewController(detailView, animated: true)
        }
    }
}

class LocationImageCell: UICollectionViewCell {
    var imageView: UIImageView!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupImageView()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupImageView()
    }

    func setupImageView() {
        imageView = UIImageView(frame: contentView.bounds)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true

        contentView.addSubview(imageView)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
        imageView.frame = contentView.bounds
    }
}

class FutureEventCell: LocationImageCell {
    var titleLabel: UILabel?

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLabel()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLabel()
    }

    func setupLabel() {
        let edge = UIEdgeInsetsMake(10, 10, 10, 10)
        titleLabel = UILabel(frame: UIEdgeInsetsInsetRect(contentView.bounds, edge))
        titleLabel?.textColor = .white
        titleLabel?.font = UIFont.systemFont(ofSize: 12.0)
        titleLabel?.adjustsFontSizeToFitWidth = true
        titleLabel?.numberOfLines = 0

        contentView.addSubview(titleLabel!)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel = nil
    }
}
