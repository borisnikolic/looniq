//
//  HomeViewController.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import UIKit
import SlideMenuControllerSwift

class HomeViewController: SlideMenuController {
    var id: Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        SlideMenuOptions.hideStatusBar = false
        SlideMenuOptions.simultaneousGestureRecognizers = false
        SlideMenuOptions.rightViewWidth = 230
        SlideMenuOptions.leftViewWidth = 230

        let listView = ListViewController()
        if let id = id {
            listView.id = id
        }
        self.mainViewController = UINavigationController(rootViewController: listView)
        self.rightViewController = FilterViewController()
        self.leftViewController = LoginViewController()

        deleteOldImages()

        super.awakeFromNib()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }

    func deleteOldImages() {
        let jsonFileUrl = URL(string: looniqUrl + "delete_old_images.php")!
        let session = URLSession(configuration: BasicAuth.config)
        session.dataTask(with: jsonFileUrl).resume()
    }
}
