//
//  FilterViewController.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import UIKit
import MapKit
import GooglePlaces

class FilterViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchControllerDelegate, UISearchBarDelegate {
    enum Order: Int {
        case distance = 0
        case genre
        case category
        case price
        case open
    }

    var currentOption = FilterOption()
    var isSectionCollapsed = [Bool]()
    var isGenreCollapsed = true
    var isCategoryCollapsed = true

    var filterTableView = UITableView()
    var searchTableViewController = SearchTableViewController()
    var searchController = UISearchController(searchResultsController: nil)

    var blurView = UIVisualEffectView()

    override func viewDidLoad() {
        super.viewDidLoad()

        isSectionCollapsed = [true, true, true, true]

        blurView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        blurView.frame = view.frame
        view.addSubview(blurView)

        let frame = CGRect(x: 0, y: 20, width: view.frame.width, height: view.frame.height)
        filterTableView = UITableView(frame: frame, style: .grouped)
        filterTableView.delegate = self
        filterTableView.dataSource = self
        filterTableView.separatorStyle = .none
        filterTableView.autoresizesSubviews = true
        filterTableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        filterTableView.isScrollEnabled = true
        filterTableView.backgroundColor = .clear

        definesPresentationContext = true

        searchTableViewController = SearchTableViewController(style: .plain)
        searchTableViewController.tableView.separatorStyle = .none
        searchTableViewController.tableView.autoresizesSubviews = true
        searchTableViewController.tableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        searchTableViewController.tableView.isScrollEnabled = true
        searchTableViewController.tableView.backgroundColor = .clear

        searchController = UISearchController(searchResultsController: searchTableViewController)
        searchController.searchResultsUpdater = self
        searchController.delegate = self
        searchTableViewController.searchController = searchController

        let searchBar = searchController.searchBar
        searchBar.searchBarStyle = .minimal
        searchBar.autocapitalizationType = .none
        searchBar.tintColor = #colorLiteral(red: 0.4117647059, green: 0.5843137255, blue: 0.8509803922, alpha: 1)
        searchBar.sizeToFit()
        searchBar.placeholder = "Ort eingeben".localized
        searchBar.delegate = self

        for subview in searchController.searchBar.subviews {
            for secondSubview in subview.subviews {
                if let searchBarTextField = secondSubview as? UITextField {
                    searchBarTextField.textColor = .white
                    break
                }
            }
        }

        filterTableView.tableHeaderView = searchBar
        filterTableView.backgroundView = UIView()

        view.addSubview(filterTableView)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if let location = LocationManager.sharedManager.simulatedLocation {
            searchController.searchBar.text = location.formattedAddress
        }

        searchController.searchBar.delegate = self
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let location = LocationManager.sharedManager.simulatedLocation {
            searchController.searchBar.text = location.formattedAddress
        }

        if searchController.searchBar.text == "" {
            LocationManager.sharedManager.simulatedLocation = nil
            LocationManager.sharedManager.startLocationUpdatesNow()
        }
        searchController.searchBar.delegate = nil
        searchController.isActive = false
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        view.frame = CGRect(x: 0, y: 0, width: 230, height: UIScreen.main.bounds.height)
        blurView.frame = view.frame
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return FilterOption.values.count + 1 // + Reset
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section < Order.open.rawValue && isSectionCollapsed[section] {
            return 1
        } else {
            if section == Order.distance.rawValue {
                return DistanceFilter.values.count + 1

            } else if section == Order.genre.rawValue {
                if isGenreCollapsed {
                    return 6 // Header + 4 cells + show all cell
                }
                return GenreFilter.values.count + 1 // +1 Header cell

            } else if section == Order.category.rawValue {
                if isCategoryCollapsed {
                    return 6
                }
                return LocationCategoryFilter.values.count + 1

            } else if section == Order.price.rawValue {
                return PriceFilter.values.count + 1

            } else if section == Order.open.rawValue {
                return 1
            }
        }
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell(style: .default, reuseIdentifier: "filterCell")
        cell.backgroundColor = .clear

        // Header cell
        if indexPath.row == 0 {
            // Reset
            if indexPath.section == FilterOption.values.count {
                cell.textLabel?.textColor = .lightGray
                cell.textLabel?.text = "Reset".localized
                return cell
            }
            if indexPath.section < Order.open.rawValue {

                // Show selected option when collapsed
                if isSectionCollapsed[indexPath.section] {
                    cell = UITableViewCell(style: .subtitle, reuseIdentifier: "subtitleCell")
                    if indexPath.section == Order.distance.rawValue {
                        cell.detailTextLabel?.text = currentOption.distance.description

                    } else if indexPath.section == Order.genre.rawValue {
                        cell.detailTextLabel?.text = currentOption.genre.description

                    } else if indexPath.section == Order.category.rawValue {
                        cell.detailTextLabel?.text = currentOption.category.description

                    } else if indexPath.section == Order.price.rawValue {
                        cell.detailTextLabel?.text = currentOption.price.description
                    }
                }

                let arrowImage = UIImage(named: "arrow")!
                let accessoryView = UIImageView(image: arrowImage)

                if isSectionCollapsed[indexPath.section] {
                    accessoryView.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI_2))
                } else {
                    accessoryView.transform = CGAffineTransform(rotationAngle: CGFloat(-M_PI_2))
                }
                accessoryView.contentMode = .center
                accessoryView.alpha = 0.8
                let accessoryWrapperView = UIView(frame: CGRect(x: 0, y: 0, width: arrowImage.size.width, height: arrowImage.size.height))
                accessoryWrapperView.addSubview(accessoryView)
                cell.accessoryView = accessoryWrapperView
            }

            if indexPath.section == Order.open.rawValue && currentOption.nowOpen {
                cell.accessoryType = .checkmark
            }

            let cellLabel = FilterOption.values[indexPath.section]
            cell.textLabel?.text = cellLabel.description

        } else if indexPath.section == Order.distance.rawValue {
            let description = DistanceFilter.values[indexPath.row-1].description
            cell.textLabel?.text = "        " + description
            if description == currentOption.distance.description {
                cell.accessoryType = .checkmark
            }

        } else if indexPath.section == Order.genre.rawValue {
            if indexPath.row == 5 && isGenreCollapsed {
                cell.textLabel?.text = "            " + "Alle anzeigen".localized
                cell.textLabel?.font = UIFont.systemFont(ofSize: 12)
                cell.textLabel?.textColor = .lightGray
                return cell
            }
            let description = GenreFilter.values[indexPath.row-1].description
            cell.textLabel?.text = "        " + description
            if description == currentOption.genre.description {
                cell.accessoryType = .checkmark
            }

        } else if indexPath.section == Order.category.rawValue {
            if indexPath.row == 5 && isCategoryCollapsed {
                cell.textLabel?.text = "            " + "Alle anzeigen".localized
                cell.textLabel?.font = UIFont.systemFont(ofSize: 12)
                cell.textLabel?.textColor = .lightGray
                return cell
            }
            let description = LocationCategoryFilter.values[indexPath.row-1].description
            cell.textLabel?.text = "        " + description
            if description == currentOption.category.description {
                cell.accessoryType = .checkmark
            }

        } else if indexPath.section == Order.price.rawValue {
            let description = PriceFilter.values[indexPath.row-1].description
            cell.textLabel?.text = "        " + description
            if description == currentOption.price.description {
                cell.accessoryType = .checkmark
            }
        }

        cell.backgroundColor = .clear
        cell.textLabel?.textColor = .white
        cell.detailTextLabel?.textColor = .lightGray

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)

        guard let label = cell?.textLabel?.text?.replacingOccurrences(of: "        ", with: "") else { return }
        let section = indexPath.section

        if indexPath.row == 0 && section < Order.open.rawValue {
            isSectionCollapsed[section] = !isSectionCollapsed[section]
            if isSectionCollapsed[section] && section == Order.genre.rawValue && !isGenreCollapsed {
                isGenreCollapsed = true
            } else if isSectionCollapsed[section] && section == Order.category.rawValue && !isCategoryCollapsed {
                isCategoryCollapsed = true
            }
            tableView.reloadSections(IndexSet(integer: section), with: .automatic)
        }

        if indexPath.row > 0 {
            if section == Order.distance.rawValue {
                if let option = DistanceFilter(string: label) {
                    currentOption.distance = option
                    tableView.reloadSections(IndexSet(integer: section), with: .automatic)
                }

            } else if section == Order.genre.rawValue {
                if indexPath.row == 5 && isGenreCollapsed {
                    isGenreCollapsed = false
                    tableView.reloadSections(IndexSet(integer: section), with: .none)
                }
                if let option = GenreFilter(string: label) {
                    currentOption.genre = option
                    tableView.reloadSections(IndexSet(integer: section), with: .automatic)
                }

            } else if section == Order.category.rawValue {
                if indexPath.row == 5 && isCategoryCollapsed {
                    isCategoryCollapsed = false
                    tableView.reloadSections(IndexSet(integer: section), with: .none)
                }
                if let option = LocationCategoryFilter(string: label) {
                    currentOption.category = option
                    tableView.reloadSections(IndexSet(integer: section), with: .automatic)
                }

            } else if section == Order.price.rawValue {
                if let option = PriceFilter(string: label) {
                    currentOption.price = option
                    tableView.reloadSections(IndexSet(integer: section), with: .automatic)
                }
            }
        }

        if indexPath.row == 0 && section == Order.open.rawValue {
            currentOption.nowOpen = !currentOption.nowOpen
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }

        if indexPath.row == 0 && section == FilterOption.values.count {
            currentOption.reset()
            searchController.searchBar.text = ""
            tableView.reloadData()
        }

        EventFilter.sharedInstance.sortOptionChanged(to: currentOption)
    }

    // MARK: - Search Controller
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text, text.characters.count > 1 else { return }
        let filter = GMSAutocompleteFilter()
        filter.type = .city
        GMSPlacesClient.shared().autocompleteQuery(text, bounds: nil, filter: filter) { results, error in
            guard error == nil, let results = results else { return }

            self.searchTableViewController.searchItems = results
            self.searchTableViewController.tableView.reloadData()
        }

    }

    func willPresentSearchController(_ searchController: UISearchController) {
        filterTableView.resignFirstResponder()
        filterTableView.isHidden = true
    }

    func didDismissSearchController(_ searchController: UISearchController) {
        filterTableView.becomeFirstResponder()
        filterTableView.isHidden = false
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        verifySearchText(searchBar.text)
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        verifySearchText(searchBar.text)
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if let place = LocationManager.sharedManager.simulatedLocation {
            searchController.searchBar.text = place.formattedAddress
        }
        verifySearchText(searchBar.text)
    }

    func verifySearchText(_ text: String?) {
        if text == "" {
            LocationManager.sharedManager.simulatedLocation = nil
            LocationManager.sharedManager.startLocationUpdatesNow()
        }
    }
}

class SearchTableViewController: UITableViewController {
    var searchItems = [GMSAutocompletePrediction]()
    var searchController = UISearchController()

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchItems.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        cell.textLabel?.textColor = .white
        cell.backgroundColor = .clear

        let regularFont = UIFont.systemFont(ofSize: UIFont.labelFontSize)
        let boldFont = UIFont.boldSystemFont(ofSize: UIFont.labelFontSize)

        let bolded = searchItems[indexPath.row].attributedFullText.mutableCopy() as! NSMutableAttributedString
        bolded.enumerateAttribute(kGMSAutocompleteMatchAttribute, in: NSMakeRange(0, bolded.length), options: []) { (value, range: NSRange, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
            let font = (value == nil) ? regularFont : boldFont
            bolded.addAttribute(NSFontAttributeName, value: font, range: range)
        }

        cell.textLabel?.attributedText = bolded

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let placeID = searchItems[indexPath.row].placeID else { return }
        GMSPlacesClient.shared().lookUpPlaceID(placeID) { place, error in
            guard error == nil, let place = place else { return }

            DispatchQueue.main.async {
                LocationManager.sharedManager.simulatedLocation = place
                tableView.resignFirstResponder()
                self.searchController.isActive = false
                self.searchController.searchBar.endEditing(true)
                self.searchController.searchBar.text = place.formattedAddress
            }
        }
    }
}
