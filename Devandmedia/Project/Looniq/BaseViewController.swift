//
//  BaseViewController.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import UIKit
import SafariServices

class BaseViewController: UIViewController, UIScrollViewDelegate, UITextViewDelegate, SFSafariViewControllerDelegate {
//    var headerImageYOffset = CGFloat()
    var completeOffset = CGFloat()
    var headerImage = UIImageView()

    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = UIRectEdge()

        if let slideController = self.slideMenuController() {
            slideController.removeLeftGestures()
            slideController.removeRightGestures()
        }
    }

    // MARK: - Scroll View
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        // Don't allow left/right scrolling
//        if (scrollView.contentOffset.x != 0) {
//            var offset = scrollView.contentOffset
//            offset.x = 0
//            scrollView.contentOffset = offset
//        }
//
//        let scrollOffset = scrollView.contentOffset.y
//        var headerImageFrame = headerImage.frame
//
//        if scrollOffset < 0 {
//            let offset = headerImageYOffset - (scrollOffset / 3)
//            headerImageFrame.origin.y = offset
//            completeOffset = offset
//        } else {
//            let offset = headerImageYOffset - (scrollOffset / 2)
//            headerImageFrame.origin.y = offset
//            completeOffset = offset
//        }
//
//        headerImage.frame = headerImageFrame
//    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateHeaderImageBounds(for: UIScreen.main.bounds.size)
    }

    func addBackgroundImageToView() {
        self.view.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.09411764706, blue: 0.07058823529, alpha: 1)
//        var image = UIImage(named: "image")!
//        completeOffset = -100.0
//        var frame = CGRect(x: 0, y: 0, width: UIScreen.main().bounds.width, height: UIScreen.main().bounds.height*2)
//        frame.origin.y = headerImageYOffset
//        headerImage = UIImageView(frame: frame)
//        headerImage.image = image
//        headerImage.contentMode = .scaleAspectFill
//        headerImage.clipsToBounds = true
//
//        self.view.addSubview(headerImage)
    }

    func updateHeaderImageBounds(for size: CGSize) {
        let frame = CGRect(x: 0, y: completeOffset, width: size.width, height: size.height*2)
        self.headerImage.frame = frame
    }

    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        if #available(iOS 9.0, *) {
            let safari = SFSafariViewController(url: URL)
            if #available(iOS 10.0, *) {
                safari.preferredBarTintColor = #colorLiteral(red: 0.1691845655, green: 0.1450073421, blue: 0.1447746456, alpha: 1)
            }
            safari.view.tintColor = #colorLiteral(red: 0.4117647059, green: 0.5843137255, blue: 0.8509803922, alpha: 1)
            safari.delegate = self
            self.present(safari, animated: true, completion: nil)
            return false
        } else {
            UIApplication.shared.openURL(URL)
            return false
        }
    }
}
