//
//  ListViewController.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import UIKit
import MBProgressHUD
import FBSDKCoreKit
import FBSDKLoginKit

class ListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchControllerDelegate, UIViewControllerPreviewingDelegate, EventDownloadDelegate, LocationUpdateDelegate, InfoBoxControllerDownloadDelegate, EventFilterDelegate, EventImageDownloadDelegate, LocationDownloadDelegate, TimeManagerDelegate {

    // MARK: - Properties
    var eventManager = EventManager()
    var tableView = UITableView()

    var searchController = UISearchController(searchResultsController: nil)

    var infos = [Info]()
    var displayEvents = [Event]()
    var searchLocations = [Location]()
    var eventsLoaded = false

    var sectionCount = Int()
    var filterOption = FilterOption()

    var failReason: String?

    var searchBarTopView = UIView()

    var id: Int?

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        eventManager.delegate = self
        eventManager.locationDelegate = self

        setupEventsAndInfoBox()
        eventManager.startLocationUpdates()

        EventFilter.sharedInstance.delegate = self

        setupNavigationController()

        searchController.searchResultsUpdater = self
        searchController.delegate = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        searchController.searchBar.searchBarStyle = .minimal
        searchController.searchBar.tintColor = #colorLiteral(red: 0.4117647059, green: 0.5843137255, blue: 0.8509803922, alpha: 1)
        searchController.searchBar.sizeToFit()
        searchController.searchBar.placeholder = "Events oder Locations suchen".localized
        searchController.searchBar.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.09411764706, blue: 0.07058823529, alpha: 1)

        // HACK: Make text in search field white
        for subview in searchController.searchBar.subviews {
            for secondSubview in subview.subviews {
                if let searchBarTextField = secondSubview as? UITextField {
                    searchBarTextField.textColor = UIColor.white
                    break
                }
            }
        }

        setupTableView()

        _ = TimeManager(target: self)

        if #available(iOS 9.0, *) {
            registerForPreviewing(with: self, sourceView: tableView)
        }

        tableView.keyboardDismissMode = .interactive

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.slideMenuController()?.addLeftGestures()
        self.slideMenuController()?.addRightGestures()
        guard let selected = tableView.indexPathForSelectedRow else { return }
        tableView.deselectRow(at: selected, animated: true)

        tableView.reloadData()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        LocationManager.sharedManager.stopLocationUpdates()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        LocationManager.sharedManager.startLocationUpdates()
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    func setupEventsAndInfoBox() {
        eventManager.setupEvents()

        let infoBoxController = InfoBoxController()
        infoBoxController.delegate = self
        infoBoxController.downloadInfos()

        MBProgressHUD.hide(for: view, animated: false)
        let progressHUD = MBProgressHUD.showAdded(to: view, animated: true)
        progressHUD.label.text = "Suche nach Events".localized
    }

    // MARK: - Time & Date Related
    func updateDate() {
        eventManager.setupEvents()
    }

    func updateClock() {
        // Update and download new events at 4 o'clock
        if TimeManager.currentLocaleIndependentTime == "04:00" {
            updateDate()
            return
        }

        // Update label at midnight
        if TimeManager.currentLocaleIndependentTime == "00:00" {
            if !isSearchActive() {
                tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
            }
        }
    }

    // MARK: - Setup Methods
    func setupTableView() {
        tableView = UITableView(frame: self.view.frame, style: .grouped)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.autoresizesSubviews = true
        tableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        tableView.isScrollEnabled = true
        tableView.backgroundColor = UIColor.clear
        tableView.registerReusableCell(DateCell.self)
        tableView.registerReusableCell(InfoCell.self)
        tableView.registerReusableCell(AddEventCell.self)
        tableView.registerReusableCell(EventCell.self)
        tableView.registerReusableCell(LocationCell.self)
        tableView.tableHeaderView = searchController.searchBar
        tableView.backgroundView = UIView()

        self.addBackgroundImageToView()

        self.view.addSubview(tableView)
    }

    func setupNavigationController() {
        title = "Looniq"

        addRightBarButtonWithImage(UIImage(imageLiteralResourceName: "filter"))

        let profilePic = FBSDKProfilePictureView(frame: CGRect(x: 0, y: 0, width: 22, height: 22))
        profilePic.profileID = "me"
        profilePic.pictureMode = .square
        addLeftBarButton(with: profilePic)

        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.backgroundColor = .black
        navigationController?.navigationBar.tintColor = .white
    }

    // MARK: - Helper Methods
    func isSearchActive() -> Bool {
        return (searchController.isActive && searchController.searchBar.text != "")
    }

    func updateTableViewForLocation() {
        tableView.beginUpdates()
        if !isSearchActive() {
            tableView.reloadSections(IndexSet(integer: 2), with: .automatic)
        } else {
            tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
        }
        tableView.endUpdates()
    }

    func updateTableViewForIndexPaths(_ indexpaths: [IndexPath]) {
        tableView.beginUpdates()
        tableView.reloadRows(at: indexpaths, with: .automatic)
        tableView.endUpdates()
    }

    func getIndexPaths(for ids: [Int]) -> [IndexPath] {
        var indexPaths = [IndexPath]()
        for id in ids {
            for i in 0..<displayEvents.count {
                if displayEvents[i].id == id {

                    var indexPath = IndexPath()
                    if !isSearchActive() {
                        indexPath = IndexPath(row: i, section: 2)
                    } else {
                        indexPath = IndexPath(row: i, section: 0)
                    }
                    indexPaths.append(indexPath)
                }
            }
        }
        return indexPaths
    }

    // MARK: - Table View
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Date
        if section == 0 && !isSearchActive() {
            return 1
        // Infobox
        } else if section == 1 && !isSearchActive() {
            return infos.count
        // Add events
        } else if section == sectionCount-1 && !isSearchActive() {
            return 1
        // Events (while search)
        } else if section == 0 && isSearchActive() {
            return displayEvents.count > 0 ? displayEvents.count : 1

        // Locations (while search)
        } else if section == 1 && isSearchActive() {
            return searchLocations.count > 0 ? searchLocations.count : 1

        // Events (not searching)
        } else {
            if !eventManager.locationEnabled {
                return 1
            } else {
                if eventsLoaded && displayEvents.count == 0 {
                    return 1
                }
                return displayEvents.count
            }
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        if isSearchActive() {
            sectionCount = 2
        } else {
            sectionCount = 4
        }

        return sectionCount
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.section == 0 && !isSearchActive() {
            if indexPath.row == 0 {
                let datecell = tableView.dequeueReusableCell(for: indexPath) as DateCell
                datecell.fill(dateLabel: TimeManager.currentDate)
                return datecell
            }

        } else if indexPath.section == 1 && !isSearchActive() {
            let info = infos[indexPath.row]

            let infoCell = tableView.dequeueReusableCell(for: indexPath) as InfoCell
            infoCell.fill(title: info.title, text: info.text)

            return infoCell

        } else if indexPath.section == (sectionCount-1) && !isSearchActive() && eventsLoaded {
            let addEventCell = tableView.dequeueReusableCell(for: indexPath) as AddEventCell
            addEventCell.fill()

            return addEventCell
        } else if !eventsLoaded {
            let cell = BaseCell()
            cell.textLabel?.text = ""
            cell.selectionStyle = .none
            cell.accessoryType = .none

            return cell

        } else {
            if !eventManager.locationEnabled {
                let cell = BaseCell()
                cell.textLabel?.text = "Aktiviere die Ortungsdienste um Events zu sehen. Einstellungen > Looniq > Standort".localized
                cell.textLabel?.numberOfLines = 0
                cell.textLabel?.lineBreakMode = .byWordWrapping
                cell.textLabel?.font = cell.textLabel?.font.withSize(15.0)
                cell.accessoryType = .disclosureIndicator

                return cell
            }

            if displayEvents.count == 0 && !isSearchActive() {
                let cell = BaseCell()
                if let reason = failReason {
                    cell.textLabel?.text = reason.localized
                } else {
                    cell.textLabel?.text = "Heute keine Events".localized
                }
                cell.selectionStyle = .none
                cell.accessoryType = .none

                cell.accessibilityIdentifier = "NoEventsCell"

                return cell
            }

            if displayEvents.count == 0 && indexPath.section == 0 && isSearchActive() {
                let cell = BaseCell()
                cell.textLabel?.text = "Keine Events gefunden".localized
                cell.selectionStyle = .none
                cell.accessoryType = .none

                cell.accessibilityIdentifier = "NoEventsCell"

                return cell
            }

            if indexPath.section == 1 && isSearchActive() {
                if searchLocations.count == 0 {
                    let cell = BaseCell()
                    cell.textLabel?.text = "Keine Locations gefunden".localized
                    cell.selectionStyle = .none
                    cell.accessoryType = .none

                    cell.accessibilityIdentifier = "NoLocationsCell"

                    return cell
                }

                let cell = tableView.dequeueReusableCell(for: indexPath) as LocationCell

                let index = indexPath.row

                guard index < searchLocations.count else { return cell }
                let location = searchLocations[index]
                cell.fill(location: location)

                return cell
            }

            let eventCell = tableView.dequeueReusableCell(for: indexPath) as EventCell

            let index = indexPath.row

            guard index < displayEvents.count else { return eventCell }
            let event = displayEvents[index]
            event.delegate = self
            event.downloadImage()
            eventCell.fill(event: event)

            return eventCell
        }

        return UITableViewCell(style: .default, reuseIdentifier: nil)
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if isSearchActive() {
            if section == 0 {
                return "Events"
            } else if section == 1 {
                return "Locations"
            }
        }
        return nil
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !eventsLoaded {
            return
        }
        if indexPath.section == sectionCount-1 && !isSearchActive() {
            let addEventView = UIStoryboard(name: "AddEvent", bundle: nil).instantiateViewController(withIdentifier: "AddEvent") as UIViewController
            navigationController?.pushViewController(addEventView, animated: true)
        } else if (indexPath.section > 1 && indexPath.section < sectionCount-1 && !isSearchActive()) || (indexPath.section == 0 && isSearchActive()) {
            if !eventManager.locationEnabled {
                guard let selected = tableView.indexPathForSelectedRow else { return }
                tableView.deselectRow(at: selected, animated: true)
                UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                return
            }

            if displayEvents.count == 0 {
                return
            }

            guard indexPath.row < displayEvents.count else { return }
            let event = displayEvents[indexPath.row]

            let detailView = UIStoryboard(name: "Detail", bundle: nil).instantiateViewController(withIdentifier: "Detail") as! DetailViewController
            detailView.details = event
            navigationController?.pushViewController(detailView, animated: true)
        } else if indexPath.section == 1 && isSearchActive() {
            let locationDetailView = UIStoryboard(name: "LocationDetail", bundle: nil).instantiateViewController(withIdentifier: "LocationDetail") as! LocationDetailViewController
            guard indexPath.row < searchLocations.count else { return }
            let location = searchLocations[indexPath.row]
            locationDetailView.location = location
            navigationController?.pushViewController(locationDetailView, animated: true)
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && !isSearchActive() {
            if indexPath.row == 0 {
                return 50.0
            } else if indexPath.row == 1 {
                return 90.0
            }
        } else if indexPath.section == 1 {
            return 70.0
        } else if indexPath.section == 2 {
            if displayEvents.count == 0 {
                return 50.0
            }
            guard indexPath.row < displayEvents.count else { return 50.0 }
            let event = displayEvents[indexPath.row]
            if event.ad {
                return 100.0
            }
        }
        return 100.0
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if !isSearchActive() {
            if section > 1 && section < sectionCount-1 {
                return 31.0
            }
        }

        if isSearchActive() {
            return 40.0
        }
        return 10.0
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 && !isSearchActive() {
            return 1.0
        }
        return 20.0
    }

    // MARK: - EventDownloadDelegate
    func eventsDownloaded(_ events: [Event]) {
        print(events)
        eventsLoaded = true

        if !isSearchActive() {
            displayEvents = events
        }

        eventManager.startLocationUpdates()

        DispatchQueue.main.async {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.tableView.reloadData()
        }

        if id != nil {
            let selectedEvent = eventManager.allFutureEvents.filter { event in
                if event.id == self.id {
                    return true
                }
                return false
            }

            id = nil

            guard let event = selectedEvent.first else {
                DispatchQueue.main.async {
                    let hud = MBProgressHUD.showAdded(to: UIApplication.shared.keyWindow!, animated: true)
                    hud.mode = .text
                    hud.isUserInteractionEnabled = false
                    hud.label.text = "Event nicht gefunden".localized
                    hud.hide(animated: true, afterDelay: 3.0)
                }
                return
            }

            let detailView = UIStoryboard(name: "Detail", bundle: nil).instantiateViewController(withIdentifier: "Detail") as! DetailViewController
            detailView.details = event
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(detailView, animated: true)
            }
        }
    }

    func eventsNotDownloaded(reason: String) {
        eventsLoaded = true

        failReason = reason

        DispatchQueue.main.async {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.tableView.reloadData()
        }
    }

    func updateEventData() {
        displayEvents = eventManager.getNearEvents(forToday: true)
        tableView.reloadData()
    }

    func updateLocationDistance(_ refreshIds: [Int], events: [Event]) {
        let indexPaths = getIndexPaths(for: refreshIds)
        if refreshIds.count > 0 {
            print("Update table view for ids: \(refreshIds)")
            print("Update table view for index paths: \(indexPaths)")
        }

        if !isSearchActive() {
            let boolArray = displayEvents.map { events.contains($0) }

            if !boolArray.contains(true) {
                displayEvents = events
                tableView.reloadData()
            }
        } else {
            guard let searchText = searchController.searchBar.text else { return }
            displayEvents = eventManager.filterEvents(for: searchText)
            searchLocations = eventManager.filterLocations(for: searchText)
        }
    }

    // MARK: - Location Download Delegate

    func locationsDownloaded(_ locations: [Location]) {
        searchLocations = locations
        if isSearchActive() {
            DispatchQueue.main.async {
                self.tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
            }
        }
    }

    // MARK: - Search Controller
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = searchController.searchBar.text else { return }
        displayEvents = eventManager.filterEvents(for: searchText)
        searchLocations = eventManager.filterLocations(for: searchText)
        tableView.reloadData()
    }

    func willPresentSearchController(_ searchController: UISearchController) {
        searchBarTopView.frame = CGRect(x: 0, y: 0, width: max(view.frame.width*2, view.frame.height*2), height: 30)
        searchBarTopView.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.09411764706, blue: 0.07058823529, alpha: 1)
        view.addSubview(searchBarTopView)

        eventManager.downloadLocations()
        eventManager.locationDownloadDelegate = self
        guard let searchText = searchController.searchBar.text else { return }
        displayEvents = eventManager.filterEvents(for: searchText)
        searchLocations = eventManager.filterLocations(for: searchText)
    }

    func didDismissSearchController(_ searchController: UISearchController) {
        searchBarTopView.removeFromSuperview()
        eventManager.locationDownloadDelegate = nil
        searchLocations = []
        updateEventData()
    }

    // MARK: - InfoBoxControllerDownloadDelegate
    func infosFinishedDownloading(_ infos: [Info]) {
        self.infos = infos
        tableView.reloadData()
    }

    // MARK: - EventFilterDelegate
    func eventFilterChanged(to option: FilterOption) {
        filterOption = option
        updateEventData()
    }

    // MARK: - Peek and Pop

    @available(iOS 9.0, *)
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard eventsLoaded, let indexPath = tableView.indexPathForRow(at: location) else { return nil }

        previewingContext.sourceRect = tableView.rectForRow(at: indexPath)

        if (indexPath.section == 2 && !isSearchActive()) || (indexPath.section == 0 && isSearchActive()) {
            guard displayEvents.count != 0, indexPath.row < displayEvents.count else { return nil }

            let event = displayEvents[indexPath.row]
            let detailView = UIStoryboard(name: "Detail", bundle: nil).instantiateViewController(withIdentifier: "Detail") as! DetailViewController
            detailView.details = event
            return detailView

        } else if indexPath.section == 3 {
            return UIStoryboard(name: "AddEvent", bundle: nil).instantiateViewController(withIdentifier: "AddEvent") as UIViewController
        }

        return nil
    }

    @available(iOS 9.0, *)
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        navigationController?.pushViewController(viewControllerToCommit, animated: true)
    }

    // MARK: - Event Download Delegate

    func imageDownloaded(for event: Event) {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }

    // MARK: - Keyboard avoid

    func keyboardDidShow(_ notification: Notification) {
        guard let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue else { return }
        let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
        tableView.contentInset = contentInsets
    }

    func keyboardWillHide(_ notification: Notification) {
        tableView.contentInset = UIEdgeInsets.zero
    }

}

extension BaseViewController {
    public func addLeftBarButton(with view: UIView) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.toggleLeft))
        view.addGestureRecognizer(tap)
        let leftButton = UIBarButtonItem(customView: view)
        navigationItem.leftBarButtonItem = leftButton
    }
}
