//
//  LocationManager.swift
//  Looniq
//
//  Copyright © 2016-2017 Looniq. Created by DevandMedia.
//

import Foundation
import MapKit
import CoreLocation
import GooglePlaces

protocol LocationManagerDelegate {
    func didUpdateLocation(_ location: CLLocation)
}

class LocationManager: NSObject {
    static let sharedManager = LocationManager()
    let locationManager = CLLocationManager()
    var simulatedLocation: GMSPlace?
    var eventDelegate: LocationManagerDelegate?
    var infoDelegate: LocationManagerDelegate?
    var timer = Timer()

    func startLocationUpdates() {
        locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
            locationManager.pausesLocationUpdatesAutomatically = true
            startLocationUpdatesNow()
            timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(LocationManager.startLocationUpdatesNow), userInfo: nil, repeats: true)
        }
    }

    func startLocationUpdatesNow() {
        locationManager.startUpdatingLocation()
    }

    func stopLocationUpdates() {
        locationManager.stopUpdatingLocation()
        timer.invalidate()
    }
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
        guard let location = manager.location else { return }
        if let simulatedLocation = simulatedLocation {
            let location = CLLocation(latitude: simulatedLocation.coordinate.latitude, longitude: simulatedLocation.coordinate.longitude)

            eventDelegate?.didUpdateLocation(location)
            infoDelegate?.didUpdateLocation(location)
            self.stopLocationUpdates()
        } else {
            eventDelegate?.didUpdateLocation(location)
            infoDelegate?.didUpdateLocation(location)
        }
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            startLocationUpdates()
        }
    }
}

class MapUIView: UIView {
    var location = Location()

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard self.bounds.contains((touches.first?.location(in: self))!) else { return }
        let latitude = location.latitude
        let longitude = location.longitude
        let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)

        let place = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
        let item = MKMapItem(placemark: place)
        item.name = location.name

        item.openInMaps(launchOptions: nil)
    }
}
